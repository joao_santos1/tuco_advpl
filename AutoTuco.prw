#Include 'Protheus.ch'
#Include 'Totvs.ch'
#Include 'Tuco.ch'

#DEFINE FASE_LOG_FONTE 1
#DEFINE FASE_PILHA 2
#DEFINE FASE_FRAME 3

//-------------------------------------------------------------------
/*/{Protheus.doc} U_splitError
Fun��o principal chamada do menu do Browse "Auto Erro"

@type       User Function

@author     Jo�o Pedro
@since      08/01/2018
@version    P12
/*/
//-------------------------------------------------------------------
User Function splitError()

    Local autoTuco := AutoTuco():New()  //Instancia a classe principal da separa��o de erro autom�tica
    autoTuco:paramAuto()                //Par�metro para abrir tela de sele��o de arquivos

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} AutoTuco
Classe principal da separa��o de erros automatico

@type       Class

@author     Jo�o Pedro
@since      08/01/2018
@version    P12
/*/
//-------------------------------------------------------------------
Class AutoTuco From Tuco

    Data aErros         as Array    //Vetor que ir� guardar as linahs do erro quando tivermos lendo o arquivo txt
    Data cRealError     as String   //Erro concatenado do arquivo txt
    Data aCutError      as Array    //Guardara as se��es do erro separadas (Erro, hora do LOG_FONTE, nome da maquina)
    Data aCutError2     as Array    //Alguns erros n�o tem fun��o ou fonte, este array serve para eles
    Data cLOG_FONTE     as String   //Variavel que guardara o nome do LOG_FONTE que deu erro
    Data cLinha         as String   //Variavel que guardara a linha que deu erro
    Data cDataFonte     as String   //Variavel que guardara a data do LOG_FONTE que deu erro
    Data cFunc          as String   //Variavel que guardara o nome da fun��o que deu erro
    Data cErro          as String   //Variavel que guardara o erro puro
    Data cPilha         as String   //Variavel que ir� guardar a pilha de chamadas
    Data cRpo           as String   //Guarda o tipo do RPO
    Data cLangRpo       as String   //Guarda a linguagem do RPO
    Data cExt           as String   //Guarda a extens�o do ambiente
    Data cSystem        as String   //Guarda o sistema operacional que gerou o erro
    Data cLOG_LIB       as String   //Guarda a vers�o da lib
    Data cLOG_SERVER    as String   //Guarda vers�o server
    Data cLOG_BANC      as String   //Guarda o LOG_BANC do cliente
    Data cDbversion     as String   //Guarda a vers�o do LOG_BANC do cliente
    Data cLOG_REL       as String   //Guarda a LOG_REL do sistema
    Data cLicense       as String   //Guarda as informa��es das licen�as
    Data cTrace         as String   //1=Utiliza DbTrace; 2=N�o utiliza DbTrace
    Data cIxBlog        as String   //1=IXBLOG ligado; 2=IXBLOG desligado
    Data cKillStack     as String
    Data cTraceStack    as String
    Data cSpecKey       as String
    Data cLogProf       as String   //1=LogProfiler ativo; 2=LogProfiler desativado
    Data cTopMemo       as String   //Define se utiliza TopMemoMega
    Data cTimeOut       as String   //1=Utiliza Timeout no sistema; 2=N�o utiliza
    Data cConsole       as String   //1=Console.log ativo; 2=Console.log desativado
    Data cDirCon        as String   //Diret�rio onde s�o gravados os console.log
    Data nMaxQry        as Integer  //Tamanho m�ximo de querys
    Data nMaxStr        as Integer  //Tamanho m�ximo de Strings
    Data cUser          as String   //Nome do usuario que gerou o erro
    Data cUserId        as String   //Id do usuario que gerou o erro
    Data cDriver        as String   //Driver de impress�o utilizado
    Data cDirDrv        as String   //Diret�rio do driver de impress�o
    Data cRdd           as String   //RDD utilizado pelo cliente
    Data cSimb1         as String   //Moeda do sistema no par�metro MV_SIMB1
    Data cSimb2         as String   //Moeda do sistema no par�metro MV_SIMB2
    Data cSimb3         as String   //Moeda do sistema no par�metro MV_SIMB3
    Data cSimb4         as String   //Moeda do sistema no par�metro MV_SIMB4
    Data cSimb5         as String   //Moeda do sistema no par�metro MV_SIMB5
    Data dDtBase        as Date     //Database do sistema no momento de gerar o erro
    Data cIp            as String   //Ip do servidor que gerou o erro
    Data nPort          as Integer  //Porta do servidor que gerou o erro
    Data cEmpresa       as String   //Nome da empresa cadastrado no SM0
    Data aRet           as Array
    Data nNovo          as Integer
    Data lCustomizado   as Boolean  //Define se h� customiza��o no erro
    Data lErro          as Boolean
    Data aAtualizados   as Array    //Guarda as funcionalidades que est�o desatualizadas
    Data lPassou        as Boolean  //Variavel que define se o loop de carregamento das variaveis foi realizado
    Data lThread        as Boolean  //Variavel que define se existe mais de 1 error.log no arquivo
    Data lErroArq       as Boolean  //Variavel que define se o arquivo deve ou n�o ser deletado
    Data nIncDtlz       as Integer
    Data nIncCustom     as Integer
    Data cErroRecno     as String

    Method New() Constructor
    Method paramAuto()              //Abre tela de sele��o de arquivos
    Method ReadArq(cFile)           //L� linha por linha do arquivo .txt
    Method CutLines(nOpc)           //Separa os dados necess�rios do arquivos em variaveis
    Method GrvTable()               //Grava a tabela LOG
    Method CriaSolucao()            //Abre janela para informar solu��o para o erro
    Method ExecNote(cMsg, cFile)    //Importa a solu��o para Notepad
    Method BuscaArq(cRef)           //Faz download de arquivos em anexo
    Method ValidDate(dData, cFase)  //Valida se tem funcionalidades desatualizadas
    Method AddInfo()
    Method ValidInfo()
    Method LimpaSujeiras()
    Method PegaRecno(cErro)
    Method GravaZZZ()

End Class

//-------------------------------------------------------------------
/*/{Protheus.doc} New
Instancia var�aveis de uso privado

@type       Method

@author     Jo�o Pedro
@since      08/01/2018
@version    P12

@return     Self
/*/
//-------------------------------------------------------------------
Method New() Class AutoTuco

    ::cTrace            := "2"
    ::cIxBlog           := "2"
    ::cKillStack        := "2"
    ::cTraceStack       := "2"
    ::cLogProf          := "2"
    ::cTopMemo          := "1"
    ::cTimeOut          := "2"
    ::cConsole          := "2"
    ::cRealError        := ""
    ::nNovo             := 0
    ::lCustomizado      := .F.
    ::lErro             := .F.
    ::cErro             := Space(200)
    ::lPassou           := .F.
    ::lThread           := .F.
    ::lErroArq          := .T.
    ::aRet              := {}
    ::cPilha            := ""
    ::aAtualizados      := {}
    ::cLinha            := ""

Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} paramAuto
Abre janela para sele��o do arquivo

@type       Method

@author     Jo�o Pedro
@since      08/01/2018
@version    P12

@return     Self
/*/
//-------------------------------------------------------------------
Method paramAuto() Class AutoTuco

    Local lChamou       := .F.                                                                              //Define se j� chamou fun��o de leitura do arquivo para chamar m�todo de exclus�o
    Local cFile         := ""                                                                               //Nome do arquivo a ser analisado
    Local oDlg
    Local cDirArq       := Space(100)                                                                       //Diret�rio onde se encontra o arquivo
    Local bDir          := {|| cDirArq := cGetFile('Arquivos de texto|*.txt',"Arquivos",1,"C:\",.F.,)}      //Abre janela para sele��o de apenas arquivos .txt
    Local lCancel       := .F.                                                                              //Define se usu�rio chamou a rotina sem informar nenhum arquivo
    Local bAction       := {|| oDlg:End() }                                                                 //Bloco a ser executado quando clicar no bot�o Ok
    Local bCancel       := {|| lCancel := .T., oDlg:End()}                                                  //Bloco a ser executado quando clicar no bot�o Cancel
    Local nLin := 25                                                                                        //Linha de inicio da janela de sele��o de arquivos
    Local nCol := 12                                                                                        //Coluna de inicio da janela de sele��o de arquivos
    Local nRecno

    Pergunte("ERRORLOG", .T.)                                                                               //Pergunte se deve incluir erros desatualizados e com customiza��o

    ::nIncDtlz := MV_PAR01                                                                                  //Seta valores do pergunte sobre fonte desatualizado
    ::nIncCustom := MV_PAR02                                                                                //Seta valores do pergunte sobre customiza��es

    DEFINE MSDIALOG oDlg TITLE "Escolha o arquivo" FROM 320, 400 TO 570, 720 OF oMainWnd PIXEL

        oPanel:= TPanel():New(0, 0, "", oDlg,, .F., .F.,,, 90, 165)
        oPanel:Align:= CONTROL_ALIGN_ALLCLIENT

        @ nLin, nCol Say "Arquivo ?" Size 160, 08 PIXEL OF oPanel
        nLin += 10
        @ nLin, nCol MsGet cDirArq Size 120, 08 PIXEL OF oPanel
        @ nLin, nCol+120 BUTTON "..." ACTION Eval(bDir) Size 10, 10 PIXEL OF oPanel

    ACTIVATE MSDIALOG oDlg ON INIT EnchoiceBar(oDlg, bAction, bCancel) CENTERED

    If !Empty(cDirArq)
        aAdd(::aRet, "\" + StrTokArr(cDirArq, "\")[Len(StrTokArr(cDirArq, "\"))])                           //Separa nome do arquivo do diret�rio
        cFile := ::aRet[1]                                                                                  //Nome do arquivo
        If ".log" $ cFile .OR. ".txt" $ cFile                                                               //Somente l� arquivo .txt ou .log
            Processa({||::ReadArq(cDirArq)},STR0006,STR0007,.F.)                                            //� dificil processar tanta coisa..
            lChamou := .T.                                                                                  //Define que chamou fun��o de leitura do arquivo
        Else
            MsgStop(STR0008,STR0002)                                                                        //Voc� n�o quer que eu leia arquivos com esta extens�o n� ?
            Return .F.
        Endif

        /* Se tiver customiza��o na pilha, por�m permitir que inclua erros com customiza��o. Somente apresenta um alerta */
        If ::lCustomizado .AND. ::nIncCustom == 1
            MsgAlert(STR0009,STR0002)                                                                       //S�rio que voc� n�o viu a customiza��o na pilha de chamadas do erro ?
        Endif

        If !::lErro .AND. lChamou .AND. ::lErroArq
            nRecno := ::PegaRecno(::cErroRecno)
            If ExistDir("\tuco_erros\"+cValToChar(nRecno)+"\")
                If !CpyT2S(cDirArq, "\tuco_erros\"+cValToChar(nRecno)+"\")
                    MsgInfo(STR0050, STR0002) //"N�o foi poss�vel copiar arquivo para o servidor"
                Endif
            Else
                If MakeDir("\tuco_erros\"+cValToChar(nRecno)+"\") == 0
                    If !CpyT2S(cDirArq, "\tuco_erros\"+cValToChar(nRecno)+"\")
                        MsgInfo(STR0050, STR0002) //"N�o foi poss�vel copiar arquivo para o servidor"
                    Endif
                Else
                    MsgStop(STR0114, STR0002) //"N�o foi poss�vel criar o diret�rio para armazenamento dos arquivos de erro"
                Endif
            Endif
            If MsgYesNo(STR0010,STR0002)                                                                    //Posso sumir com este arquivo ?
                FErase(cDirArq)                                                                               //Apaga o arquivo do diret�rio local
            Endif
            TMP->(DbCloseArea())
        Endif
    Elseif !lCancel
        MsgStop(STR0077, STR0002)                                                                           //Voc� n�o informa nenhum arquivo e quer que eu processe o que ?
    Endif

Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} ReadArq
L� linha por linha do arquivo

@type       Method

@author     Jo�o Pedro
@since      12/12/2018
@version    P12

@param      cFile {String}, Diret�rio do arquivo a ser analisado

@return     Self
/*/
//-------------------------------------------------------------------
Method ReadArq(cFile) Class AutoTuco

    Local oFile                                                                 //Objeto que conter� o FWFileReader
    Local cLinha                                                                //Guarda a linha atual do arquivo
    Local nVez                                                                  //Variavel necess�ria para que n�o assimile varias linhas ao vetor do erro
    Local lChamou := .F.                                                        //Define se chamou fun��o que separa dados do arquivo em variaveis

    /* Necess�rio para n�o haver erro ao incluir 2 arquivos seguidos */
    ::cErro := ""
    ::aErros := {}

    ProcRegua(1000)

    oFile := FWFileReader():New(cFile)                                          //Instancia a classe de leitura do arquivo
    If oFile:Open()                                                             //Abre o arquivo para leitura
        Do While oFile:hasLine()                                                //Executa o loop at� acabar os dados do arquivo
            IncProc()
            cLinha := oFile:GetLine()                                           //L� a linha atual
            If "��" $ cLinha                                                    //Alguns erros vem com este caractere quando executamos a classe, nesse caso eles devem ser inclu�dos manualmente
                MsgStop(STR0011,STR0002)                                        //Estou muito cansado. inclua o erro manualmente
                ::lErro := .T.
                ::lErroArq := .F.
                Return .F.
            Endif
            nVez := 0
            If "THREAD ERROR" $ cLinha .OR. "ERRO THREAD" $ cLinha              //Se for encontrado "THREAD ERROR" adiciona ao vetor aErros
                if ::lThread
                    MsgStop(STR0012 + CRLF + STR0013, STR0002)                  //Hey meu amigo(a), eu sou uma m�quina, mas n�o abusa n�o..
                    ::lErroArq := .F.
                    Return .F.
                Else
                    AADD(::aErros, cLinha)                                      //Adiciona a linha atual do arquivo ao vetor
                    nVez := 1
                    ::lThread := .T.                                            //Define que j� passou no Thread Erro (Come�o do arquivo)
                Endif
            Endif
            If " on " $ cLinha .AND. !" on (" $ cLinha                          //Quando for encontrado " on " na linha, quer dizer que chegamos no final do erro puro
                If aScan(::aErros, "THREAD ERROR") > 0 .OR. aScan(::aErros, "ERRO THREAD") .OR. aScan(::aErros, "﻿THREAD ERROR") > 0 .AND. nVez == 0  //Verifica se j� passou no come�o do arquivo
                    AADD(::aErros, cLinha)
                    nVez := 1
                Endif
                If !lChamou
                    Processa({||::CutLines(1)},STR0006,STR0014)                 //Chama a fun��o para separar os dados do erro dos dados do 
                    lChamou := .T.                                              //Seta que chamou a fun��o de separar dados do erro
                Endif
            Endif
            If nVez == 0 .AND. (aScan(::aErros, "THREAD ERROR") > 0 .OR. aScan(::aErros, "ERRO THREAD") > 0) //Adiciona no vetor at� encontrar a palavra " on "
                AADD(::aErros, cLinha)
            Endif
            If nVez == 0 .AND. aScan(::aErros, "﻿THREAD ERROR") > 0           //Alguns erros vem com o Thread Erro diferente, neste caso deve adicionar mesmo assim
                AADD(::aErros, cLinha)
            Endif
        Enddo

        Begin Transaction
            Processa({||::CutLines(2)},STR0006,STR0014)                             //Processa dados da pilha de chamada
            Processa({||::CutLines(3)},STR0006,STR0014)                             //Processa dados gen�ricos
            Processa({||::GrvTable()},STR0006,STR0015)                              //Grava dados na tabela LOG
        End Transaction

        /* Necess�rio para que seja poss�vel incluir mais de um erro por sess�o */
        ::cRealError := ""
        ::cErroRecno := ::cErro
        ::cErro := ""
        ::aErros := {}

        oFile:Close()                                                           //Fecha o arquivo
    Else
        MsgStop(STR0078, STR0002)                                               //Tem algum problema com esse arquivo, n�o consegui nem abrir ele
    Endif

Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} CutLines
Separa dados do arquivo em variaveis

@type       Method

@author     Jo�o Pedro
@since      12/12/2018
@version    P12

@param      nOpc {Numeric}, Op��o de separa��o (1-Normal, 2-Pilha de chamada e 3-Gen�rico)

@return     Self
/*/
//-------------------------------------------------------------------
Method CutLines(nOpc) Class AutoTuco

    Local aAux      := {}                                                                                                   //Vetor auxiliar na quebra de linha
    Local cAux                                                                                                              //String auxiliar para ajudar a guardar as informa��es
    Local x, y, z                                                                                                           //Variaveis locais do For
    Local cGen      := ""                                                                                                   //Auxiliar na separa��o dos dados
    Local lPdRodar                                                                                                          //Variavel que define se ainda deve rodar para n�o dar error.log ao gravar nome do usuario do arquivo
    Local aOracle   := {}                                                                                                   //Necess�rio para que seja poss�vel gravar dados da pilha de chamada e gen�ricos de erros em Oracle

    /* Necess�rio para n�o dar error.log no momento de gravar o nome do usu�rio */
    If !Empty(::aErros)
        lPdRodar := Len(::aErros) >= 2 .OR. ".PRW" $ ::aErros[1] .OR. ".PRG" $ ::aErros[1] .OR. ".PRX" $ ::aErros[1]
    Else
        lPdRodar := .F.
    Endif

    ProcRegua(Len(::aErros))

    If Empty(::cRealError) .AND. lPdRodar                                                                                   //Somente grava o erro se a variavel estiver vazia, para n�o dar inconsist�ncia em erros gravados na mesma instancia
        For x := 2 To len(::aErros)
            ::cRealError += ::aErros[x] + " "                                                                               //Concatena a string do erro
        Next x

        If "line" $ ::cRealError
            ::cLinha := Alltrim(StrTokArr(StrTokArr2(::cRealError, "line")[2], ":")[2])                                       //Grava a linha que ocorreu o erro
        Endif

        /* Necess�rio para que n�o ocorra problema quando houve "on" no erro */
        If " on (" $ ::cRealError
            ::cRealError := StrTran(Alltrim(::cRealError)," on ("," in (")
        Endif

        If " on *" $ ::cRealError
            ::cRealError := StrTran(Alltrim(::cRealError)," on *"," in *")
        Endif

        If " on +" $ ::cRealError
            ::cRealError := StrTran(Alltrim(::cRealError)," on +"," in +")
        Endif

        If " on -" $ ::cRealError
            ::cRealError := StrTran(Alltrim(::cRealError)," on -"," in -")
        Endif

        If " on /" $ ::cRealError
            ::cRealError := StrTran(Alltrim(::cRealError)," on /"," in /")
        Endif
        
        If ".PRW" $ Upper(::cRealError) .OR. ".PRG" $ Upper(::cRealError) .OR. ".PRX" $ Upper(::cRealError)
            ::aCutError := StrTokArr2(::cRealError, " on ")                                                             //Separa o erro das informa��es do Fonte
        Else
            ::aCutError := StrTokArr2(::cRealError, " on ")
            ::cErro := ::cRealError                                                                                     //Se n�o existir fonte, pode gravar o erro direto na variavel
        Endif
        If "{|" $ ::aCutError[2] .OR. "{ |" $ ::aCutError[2] .AND. ::nNovo == 0
            ::aCutError2 := StrTokArr(::aCutError[2],"}")                                                               //Ir� conter as informa��es quando n�o houver fun��o
        Endif
    Endif

    /* Trata as informa��es dos Fontes */
    If nOpc == FASE_LOG_FONTE .AND. lPdRodar
        IncProc()
        If Empty(::aCutError2)
            aAux := StrTokArr(::aCutError[2],"(")                                                                       //Separa a fun��o que deu o erro
            aAux := StrTokArr(aAux[1],")")                                                                              //Separa a fun��o que deu o erro
            ::cFunc := Iif(".PRW" $ Upper(::aCutError[2]),aAux[1],"Sem Fun��o")                                         //Nome da fun��o que deu o erro
            If ".PRX" $ Upper(::aCutError[2]) .OR. ".PRW" $ Upper(::aCutError[2]) .OR. ".PRG" $ Upper(::aCutError[2])
                ::cFunc := aAux[1]                                                                                      //Grava a fun��o que estorou o erro
            Endif
            If "U_" $ ::cFunc                                                                                           //Necess�rio verifica��o se o problema ocorreu em uma customiza��o
                ::lCustomizado := .T.
            Endif
            cAux := ::aCutError[2]                                                                                      //Guarda a string que n�o foi separada na string auxiliar
            aAux := StrTokArr(cAux,")")
            If Len(aAux) >= 2
                cGen := aAux[2]
                aAux := StrTokArr(aAux[1],"(")
                If ".PRX" $ Upper(aAux[2]) .OR. ".PRW" $ Upper(aAux[2]) .OR. ".PRG" $ Upper(aAux[2])
                    ::cLOG_FONTE := StrTokArr(Alltrim(aAux[2]),"(")[1]                                                  //Retorna o nome do fonte que deu erro
                Else
                    ::cLOG_FONTE := "Sem Fonte"                                                                         //Se n�o tiver fonte, gravar descri��o gen�rica
                Endif
            Endif
            If Len(aAux) < 2
                ::cLOG_FONTE := "Sem Fonte"
            Endif
            If !Empty(cGen)
                ::cDataFonte := StrTran(Alltrim(Substr(cGen,1,11)),"/","-")                                             //Retorna a data do fonte que deu erro
            Else
                ::cDataFonte := "01/01/1900"                                                                            //Se n�o tiver data, gravar uma data gen�rica
            Endif
            ::ValidDate(ctod(::cDataFonte), "Fonte")                                                                    //Valida se o fonte est� atualizado
            ::cErro := StrTran(::aCutError[1],"'"," ")                                                                  //Grava o erro
        Else
            aAux := StrTokArr(::aCutError2[2],"(")                                                                      //Separa a fun��o que deu o erro
            aAux := StrTokArr(aAux[1],")")                                                                              //Separa a fun��o que deu o erro
            ::cFunc := "Sem fun��o"                                                                                     //Se n�o tiver fun��o grava uma fun��o gen�rica
            cGen := aAux[2]
            ::cLOG_FONTE := aAux[1]                                                                                     //Retorna o nome do fonte que deu erro
            ::cDataFonte := StrTran(Alltrim(Substr(aAux[2],1,11)),"/","-")                                              //Retorna a data do fonte que deu erro
            If Empty(::cErro)
                ::cErro := Substr(StrTran(::aCutError[1],"'"," "),0,100)                                                //Grava o erro
            Endif
        Endif

        /* Valida��o necess�ria para bancos Oracle - Sugest�o Werllen */
        If "THREAD ID" $ Upper(::cErro)
            aOracle := StrTokArr2(Upper(::cErro),"THREAD ID")                                                           //Separa os dados especificos do banco Oracle
            ::cErro := aOracle[1]                                                                                       //Grava o erro
        Endif

        ::LimpaSujeiras()

        ::nNovo := 1
    Elseif nOpc == FASE_PILHA
        IncProc()
        For y := 1 To Len(::aErros)
            IncProc()
            If "Called" $ ::aErros[y]                                                                                   //Somente grava se tiver "Called" na linha atual
                If "U_" $ ::aErros[y]                                                                                   //Verifica se tem customiza��o na pilha
                    ::lCustomizado := .T.
                Endif
                ::cPilha += StrTran(::aErros[y],"'", " ") + CRLF                                                         //Substitui qualquer caracter ' na pilha de chamada por um espa�o, para que n�ao ocorra erro no sql
            Endif
        Next y
    Elseif nOpc == FASE_FRAME                                                                                           //Adiciona as informa��es de LOG_LIB, LOG_DBACES e LOG_SERVER as variaveis
        For z := 1 To Len(::aErros)
            IncProc()
            Do Case

                Case "RPODB" $ ::aErros[z]
                    ::cRpo := Alltrim(Left(Right(Alltrim(::aErros[z]),5),4))                                            //Grava o banco do cliente

                Case "RPOLanguage" $ ::aErros[z]                                                                        //Linguagem do RPO
                    If "NENHUM" $ Upper(::aErros[z])
                        ::cLangRpo := ""
                    Else
                        ::cLangRpo := Alltrim(Left(Right(Alltrim(::aErros[z]),11),10))
                        If "portuguese" $ ::cLangRpo
                            ::cLangRpo := "Portugu�s"
                        Elseif "english" $ ::cLangRpo
                            ::cLangRpo := "Ingl�s"
                        Elseif "spanish" $ ::cLangRpo
                            ::cLangRpo := "Espanhol"
                        Endif
                    Endif

                Case "LocalDBExtension" $ ::aErros[z]                                                                   //Extens�o dos arquivos do dicion�rio
                    If "NENHUM" $ Upper(::aErros[z])
                        ::cExt := ""
                    Else
                        ::cExt := Alltrim(Left(Right(Alltrim(::aErros[z]),5),4))
                    Endif

                Case "Remote type" $ ::aErros[z]                                                                        //Sistema operacional
                    ::cSystem := Alltrim(Left(Right(Alltrim(::aErros[z]),21),20))
                    If "Windows" $ ::cSystem
                        ::cSystem := "Microsoft Windows"
                    Elseif "Linux" $ ::cSystem
                        ::cSystem := "GNU/Linux"
                    Endif

                Case "Remote Build" $ ::aErros[z]                                                                       //Lib
                    ::cLOG_LIB := Alltrim(Left(Right(Alltrim(::aErros[z]),22),21))
                    ::ValidDate(sTod(Right(::cLOG_LIB, 8)), "LIB")                                                      //Valida se a Lib esta desatualizada

                Case "Server Build" $ ::aErros[z]                                                                       //AppServer
                    ::cLOG_SERVER := StrTran(Alltrim(StrTokArr(::aErros[z], ":")[2]), "]", "")
                    ::ValidDate(sTod(Right(::cLOG_SERVER,8)), "Appserver")                                              //Valida se o Appserver esta desatualizado

                Case "DBAccess DB" $ ::aErros[z]                                                                        //Tipo do Banco (MSSQL)
                    ::cLOG_BANC := Alltrim(Left(Right(Alltrim(::aErros[z]),7),6))
                    If "DB2" $ ::cLOG_BANC
                        ::cLOG_BANC := Alltrim(StrTokArr(::cLOG_BANC,":")[2])
                    Endif

                Case "DBAccess API Build" $ ::aErros[z]                                                                 //Data do DbAccess
                    If "-" $ Alltrim(::aErros[z])
                        ::cDbVersion := Alltrim(Left(Right(Alltrim(::aErros[z]),18),17))
                        ::ValidDate(Stod(left(StrTokArr(Alltrim(Left(Right(Alltrim(::aErros[z]),18),17)), "-")[2], 8)), "DbAccess") //Valida se o DbAccess est� desatualizado
                    Else
                        ::cDbVersion := Alltrim(StrTran(StrTokArr(Alltrim(::aErros[z]), ":")[2], "]"))
                        ::ValidDate(Stod(Alltrim(StrTran(StrTokArr(Alltrim(::aErros[z]), ":")[2], "]"))), "DbAccess")               //Valida se o DbAccess est� desatualizado
                    Endif
                
                Case "RPO Release" $ ::aErros[z]                                                                        //Release
                    ::cLOG_REL := Alltrim(Left(Right(Alltrim(::aErros[z]),9),8))
                
                Case "License Server Version" $ ::aErros[z]                                                             //Licen�as
                    ::cLicense := StrTran(Alltrim(StrTokArr(::aErros[z], ":")[2]), "]", "")
                    If "LOCK" $ Upper(::cLicense)
                        ::cLicense := "Licen�as por Hardlock"
                    Elseif "License" $ ::cLicense
                        ::cLicense := "License Server Virtual " + Right(::cLicense,4)
                    Elseif Left(::cLicense,2) == "ER"
                        ::cLicense := Alltrim(StrTran(::cLicense,"ER"))
                    Endif

                Case "TOTVS Environment Trace" $ ::aErros[z]                                                            //DbTrace
                    ::cTrace := Iif("1" $ ::aErros[z], "1", "2")

                Case "TOTVS Environment IBXLog" $ ::aErros[z]                                                           //IXBLOG
                    ::cIxBlog := Iif("Nenhum" $ ::aErros[z], "2", "1")

                Case "TOTVS Environment KillStack" $ ::aErros[z]                                                        //KillStack
                    ::cKillStack := Iif("Nenhum" $ ::aErros[z], "2", "1")

                Case "TOTVS Environment TraceStack" $ ::aErros[z]                                                       //TraceStack
                    ::cTraceStack := Iif("Nenhum" $ ::aErros[z], "2", "1")

                Case "TOTVS Environment SpecialKey" $ ::aErros[z]                                                       //SpecialKey
                    ::cSpecKey := StrTran(Alltrim(StrTokArr(::aErros[z], ":")[2]), "]", "")

                Case "TOTVS Environment LogProfiler" $ ::aErros[z]                                                      //LogProfiler
                    ::cLogProf := Iif("Nenhum" $ ::aErros[z], "2", "1")

                Case "TOTVS Environment TopMemoMega" $ ::aErros[z]                                                      //TopMemoMega
                    ::cTopMemo := Iif(Val(StrTran(Alltrim(StrTokArr(::aErros[z], ":")[2]), "]", "")) == 1, "2", Iif(Val(StrTran(Alltrim(StrTokArr(::aErros[z], ":")[2]), "]", "")) == 0, "1", "3"))

                Case "TOTVS Environment ConnectionTimeOut" $ ::aErros[z]                                                //TimeOut
                    ::cTimeOut := Iif("Nenhum" $ ::aErros[z], "2", "1")

                Case "TOTVS Environment General ConsoleLog" $ ::aErros[z]                                               //ConsoleLog
                    ::cConsole := Iif("1" $ ::aErros[z], "1", "2")

                Case "TOTVS Environment General ConsoleFile" $ ::aErros[z]                                              //ConsoleFile
                    ::cDirCon := Iif(StrTran(Alltrim(StrTokArr(::aErros[z], ":")[2]), "]", "") == "Nenhum", "", Iif(Len(StrTokArr(::aErros[z],":")) >= 3, StrTran(Alltrim(StrTokArr(::aErros[z], ":")[3]), "]", ""), StrTran(Alltrim(StrTokArr(::aErros[z], ":")[2]), "]", "")))

                Case "TOTVS Environment General MaxQuerySize" $ ::aErros[z]                                             //MaxQuerySize
                    ::nMaxQry := Iif("Nenhum" $ ::aErros[z], 15960, Val(StrTran(Alltrim(StrTokArr(::aErros[z], ":")[2]), "]", "")))

                Case "TOTVS Environment General MaxStringSize" $ ::aErros[z]                                            //MaxStringSize
                    ::nMaxStr := StrTran(Alltrim(StrTokArr(::aErros[z], ":")[2]), "]", "")

                Case "CUSERNAME" $ ::aErros[z]                                                                          //Nome de usu�rio
                    If Len(StrTokArr(::aErros[z], ":")) >= 3
                        ::cUser := Alltrim(StrTokArr(::aErros[z], ":")[3])
                    Endif

                Case "__CUSERID" $ ::aErros[z]                                                                          //Id do usu�rio
                    If Len(StrTokArr(::aErros[z], ":")) >= 3
                        ::cUserId := Val(Alltrim(StrTokArr(::aErros[z], ":")[3]))
                    Endif

                Case "__DRIVER" $ ::aErros[z]                                                                           //Driver de impress�o
                    ::cDriver := Alltrim(StrTokArr(::aErros[z], ":")[3])

                Case "__RELDIR" $ ::aErros[z]                                                                           //Diret�rio do driver de impress�o
                    ::cDirDrv := Alltrim(StrTokArr(::aErros[z], ":")[3])

                Case "__CRDD" $ ::aErros[z]                                                                             //RDD do ambiente
                    ::cRdd := Alltrim(StrTokArr(::aErros[z], ":")[3])

                Case "MV_MOEDA1" $ ::aErros[z]                                                                          //Conteudo MV_SIMB1
                    ::cSimb1 := Alltrim(StrTokArr(::aErros[z], ":")[3])

                Case "MV_MOEDA2" $ ::aErros[z]                                                                          //Conteudo MV_SIMB2
                    ::cSimb2 := Alltrim(StrTokArr(::aErros[z], ":")[3])

                Case "MV_MOEDA3" $ ::aErros[z]                                                                          //Conteudo MV_SIMB3
                    ::cSimb3 := Alltrim(StrTokArr(::aErros[z], ":")[3])

                Case "MV_MOEDA4" $ ::aErros[z]                                                                          //Conteudo MV_SIMB4
                    ::cSimb4 := Alltrim(StrTokArr(::aErros[z], ":")[3])

                Case "MV_MOEDA5" $ ::aErros[z]                                                                          //Conteudo MV_SIMB5
                    ::cSimb5 := Alltrim(StrTokArr(::aErros[z], ":")[3])

                Case "DMDIDATABASE" $ ::aErros[z]                                                                       //DataBase do sistema na gera��o do erro
                    ::dDtBase := Stod(StrTran(Alltrim(StrTokArr(::aErros[z], ":")[3]), "/", ""))

                Case "CSERVERIP" $ ::aErros[z]                                                                          //Ip do servidor
                    ::cIp := Alltrim(StrTokArr(::aErros[z], ":")[3])

                Case "NPORT" $ ::aErros[z]                                                                              //Porta do servidor
                    ::nPort := Val(Alltrim(StrTokArr(::aErros[z], ":")[3]))

                Case "M0_NOMECOM(C)" $ ::aErros[z]                                                                       //Nome da empresa
                    ::cEmpresa := Alltrim(StrTokArr(::aErros[z], ":")[3])
            EndCase
        Next z
    Endif
    IncProc()

Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} GrvTable
Grava os dados do erro na tabela LOG

@type       Method

@author     Jo�o Pedro
@since      12/12/2018
@version    P12

@return     Logical, Incluir com sucesso os dados na tabela {lOk}
/*/
//-------------------------------------------------------------------
Method GrvTable() Class AutoTuco

    Local lOk := .F.                                //Define se incluiu os dados com sucesso na tabela
    Local nSoma := 0                                //Loop para definir Space de busca
    Local nSpace := 0                               //Space de busca para complementar DbSeek
    Local x                                         //Variavel local de loop
    Local cTitAtu := STR0076 + CRLF + CRLF          //FUNCIONALIDADES DESATUALIZADAS
    Local cAtualizado := ""                         //Grava funcionalidades que est�o desatualizadas

    /* Executa loop para preencher o Space para que o DbSeek funcione */
    If Len(::cErro) < 200
        While nSpace != 200
            nSoma++
            If Len(::cErro) + nSoma >= 200
                nSpace := 200
            Endif
        End
    Endif

        ProcRegua(RecCount())
        LOG->(DbGoTop())
        LOG->(DbSetOrder(2))

        While LOG->(!Eof())
            IncProc()
            If LOG->(DbSeek(xFilial()+Iif(" in " $ ::cErro, StrTran(Iif(Len(::cErro) > 200,Substr(::cErro,0,200),::cErro), " in ", " on "),  Iif(Len(::cErro) > 200,Substr(::cErro,0,200),::cErro))+ Space(nSoma)+::cFunc)) //Verifica se o erro j� existe na base de dados
                lOk := .T.
                If !Empty(LOG->LOG_SOL) .OR. !Empty(LOG->LOG_REFARQ)                                                        //Se j� existir uma solu�ao
                    Iif(!Empty(LOG->LOG_SOL),EECVIEW(LOG->LOG_SOL,"Solucao"),EECVIEW(LOG->LOG_SOL,"Referencia Arquivo"))    //Se existir solu��o. apresenta ela na tela
                    If MsgYesNo(STR0016, STR0002)                                                                           //Quer adicionar uma solu��o para esse erro ?
                        ::CriaSolucao()                                                                                     //Fun��o de inclus�o de solu��o
                        Return .T.
                    Else
                        Return .F.
                    Endif
                Else
                    If MsgYesNo(STR0017,STR0019)                                                                            //Eu achei um erro igual a esse na base, mas ainda n�o tem solu��o. Adiciona uma solu��o para ele, por favor ?
                        ::CriaSolucao()
                        Return .T.
                    Else
                        Return .F.
                    Endif
                Endif
            Endif
            LOG->(DbSkip())
        End

        /* Verifica se existe alguma funcionalidade desatualizada */
        For x := 1 To Len(::aAtualizados)
            If !Empty(::aAtualizados[x][2])                                                                                 //Necess�rio para que n�o seja apresentado tratamento para datas em branco
                cAtualizado += ::aAtualizados[x][1] + " -------> " + dToc(::aAtualizados[x][2]) + CRLF
            Endif
        Next

        if !Empty(cAtualizado)
            EECVIEW(cTitAtu + cAtualizado,STR0020)                                                                          //Apresenta os fontes que est�o desatualizados em tela
            if ::nIncDtlz == 2
                MsgStop(STR0021, STR0002)                                                                                   //Esse erro parece um museu, olha o tanto de coisa antiga. Atualize esses dados e depois volte para eu analisar novamente
                ::lErroArq := .F.
                Return .F.
            Endif
        Endif

        /* Caso haja customiza��o na pilha de chamada o sistema n�o permite inclus�o do erro */
        If ::lCustomizado .AND. ::nIncCustom == 2
            MsgStop(STR0009,STR0002)                                                                                        //S�rio que voc� n�o viu a customiza��o na pilha de chamadas do erro ?
            ::lErroArq := .F.
            Return .F.
        Endif

        /* Ponto de entrada para modificar dados na grava��o do erro */
        If ExistBlock("TUCOPE")
            Execblock("TUCOPE",.F.,.F., "ANTES_GRAVA")
        Endif

        If !lOk
            If MsgYesNo(STR0022)                                                                                            //Nunca vi isso na minha vida. Posso adicionar a base ?
                If RecLock("LOG",.T.)                                                                                       //Grava o erro na tabela LOG
                    LOG->LOG_FILIAL     := xFilial()
                    LOG->LOG_ERRO       := Iif(" in " $ ::cErro, StrTran(Iif(Len(::cErro) > 200,Substr(::cErro,0,200),::cErro), " in ", " on "),  Iif(Len(::cErro) > 200,Substr(::cErro,0,200),::cErro))
                    LOG->LOG_FUNC       := ::cFunc
                    LOG->LOG_FONTE      := ::cLOG_FONTE
                    LOG->LOG_DT_FT      := cTod(::cDataFonte)
                    LOG->LOG_TP_BC      := ::cRpo
                    LOG->LOG_LINGUA     := ::cLangRpo
                    LOG->LOG_EXT        := ::cExt
                    LOG->LOG_SYSTEM     := ::cSystem
                    LOG->LOG_LIB        := ::cLOG_LIB
                    LOG->LOG_SERVER     := ::cLOG_SERVER
                    LOG->LOG_BANC       := ::cLOG_BANC
                    LOG->LOG_DBACES     := ::cDbVersion
                    LOG->LOG_REL        := ::cLOG_REL
                    LOG->LOG_LIC        := ::cLicense
                    LOG->LOG_PILHA      := ::cPilha
                    LOG->LOG_LINHA      := Val(Alltrim(::cLinha))
                    LOG->LOG_TRACE      := ::cTrace
                    LOG->LOG_IXBLOG     := ::cIxBlog
                    LOG->LOG_KSTACK     := ::cKillStack
                    LOG->LOG_TSTACK     := ::cTraceStack
                    LOG->LOG_SPECL      := ::cSpecKey
                    LOG->LOG_PROFIL     := ::cLogProf
                    LOG->LOG_TOPMEM     := ::cTopMemo
                    LOG->LOG_TIMEOU     := ::cTimeOut
                    LOG->LOG_CONSOL     := ::cConsole
                    LOG->LOG_CONFIL     := ::cDirCon
                    LOG->LOG_MAXQRY     := ::nMaxQry
                    If !Empty(::nMaxStr)
                        LOG->LOG_MAXSTR := Val(::nMaxStr)
                    Endif
                    LOG->LOG_USER       := ::cUser
                    LOG->LOG_USERID     := ::cUserId
                    LOG->LOG_DRIVER     := ::cDriver
                    LOG->LOG_LCLDRV     := ::cDirDrv
                    LOG->LOG_RDD        := ::cRdd
                    LOG->LOG_SIMB1      := ::cSimb1
                    LOG->LOG_SIMB2      := ::cSimb2
                    LOG->LOG_SIMB3      := ::cSimb3
                    LOG->LOG_SIMB4      := ::cSimb4
                    LOG->LOG_SIMB5      := ::cSimb5
                    LOG->LOG_DTBASE     := ::dDtBase
                    LOG->LOG_IP         := ::cIp
                    LOG->LOG_PORT       := Iif(::nPort != Nil .AND. Len(cValToChar(::nPort)) > 4, Val(Left(cValToChar(::nPort),4)), ::nPort)
                    LOG->LOG_EMP        := ::cEmpresa
                    LOG->(MsUnlock())
                    MsgInfo(STR0023, STR0002)                                                                               //Depois de muita luta eu consegui incluir os dados nessa base bagun�ada
                    ::GravaZZZ()
                Endif
            Else
                Return .F.
            Endif
        Endif

        ::ValidInfo() //Chama a fun��o para apresentar as funcionalidades que n�o foram encontradas

Return lOk

//-------------------------------------------------------------------
/*/{Protheus.doc} CriaSolucao
Abre tela para informar solu��o para o erro

@type       Method

@author     Jo�o Pedro
@since      12/12/2018
@version    P12

@return     Self
/*/
//-------------------------------------------------------------------
Method CriaSolucao() Class AutoTuco

    Local cLOG_SOL := Alltrim(LOG->LOG_SOL)                                         //Variavel que ir� guardar a Solu��o
    Local cLabel := ""
    Local oMemo
    Local oFont := TFont():New("Courier New",09,15)                                 //Fonte para as palavras da janela
    Local bOk      := {|| nOpc := 1,oDlg:End()}                                     //Bloco a ser executado quando clicado no bot�o Ok
    Local bCancel  := {|| oDlg:End()}                                               //Bloco a ser executado quando clicado no bot�o Cancel
    Local aButtons := {}                                                            //Bot�es da janela de cria��o de solu��o
    Local nOpc := 0

    /* Bot�es da janela de solu��o */
    AADD(aButtons,{"NOTE",{|| ::ExecNote(LOG->LOG_SOL,"Solucao.txt")},"Notepad"})   //Exporta a solu��o para .txt
    AADD(aButtons,{"FILES",{|| ::BuscaArq(LOG->LOG_REFARQ)},"Patchs"})              //Busca os arquivos gravados para este erro

    DEFINE MSDIALOG oDlg TITLE STR0024 FROM 9,0 TO 39,85 OF oDlg                    //Inclus�o Solu��o

        oPanel := TPanel():New(0,0,"",oDlg,,.F.,.F.,,,90,165)
        oPanel:Align := CONTROL_ALIGN_ALLCLIENT

        @ 05,05 TO 190,330 Label cLabel PIXEL OF oPanel
        @ 15,10 GET oMemo Var cLOG_SOL MEMO HSCROLL FONT oFont SIZE 315,169 OF oPanel PIXEL

        oMemo:lWordWrap := .T.                                                      //Quebra linha
        oMemo:EnableVScroll(.T.)                                                    //Permite dar um Scroll na tela quando tiver muitas palavras
        oMemo:EnableHScroll(.T.)

    ACTIVATE MSDIALOG oDlg ON INIT EnchoiceBar(oDlg,bOk,bCancel,,aButtons) CENTERED

    If nOpc == 1
        If !Empty(cLOG_SOL)
            If RecLock("LOG",.F.)
                LOG->LOG_SOL := Alltrim(cLOG_SOL)                                   //Grava a solu��o na tabela
            Endif
        Endif
    Else
        If !MsgYesNo(STR0025,STR0002)                                               //Solucao n�o informada, deseja prosseguir ?
            ::CriaSolucao()                                                         //Chama novamente m�todo de cria��o de solu��o
        Else
            U_Tuco()                                                                //Volta ao Browse principal
        Endif
    Endif

Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} ExecNote
Transporta solu��o para Txt

@type       Method

@author     Jo�o Pedro
@since      13/12/2018
@version    P12

@param      cMsg    {String}, Texto a ser apresentado no arquivo
@param      cFile   {String}, Nome do arquivo a ser gerado

@return     Self
/*/
//-------------------------------------------------------------------
Method ExecNote(cMsg,cFile) Class AutoTuco

    Local cDir := GetWinDir()+"\Temp\"      //Pasta onde o arquivo ser� gerado
    Local hFile                             //Handle do arquivo

    Begin Sequence
        hFile := FCreate(cDir+cFile)        //Cria arquivo .txt

        FWrite(hFile,cMsg,Len(cMsg))        //Copia o valor da variavel cMsg para o arquivo

        FClose(hFile)                       //Fecha o arquivo

        WinExec("Notepad " + cDir+cFile)    //Abre no Notepad
    End Sequence

Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} BuscaArq
Faz download do arquivo de anexo que est� no servidor

@type       Method

@author     Jo�o Pedro
@since      13/12/2018
@version    P12

@param      cRef    {String}, Nome do arquivo a ser buscado

@return     Self
/*/
//-------------------------------------------------------------------
Method BuscaArq(cRef) Class AutoTuco

    Local aFile := {}                                                   //Contem o diretorio e nome do arquivo separados em um vetor

    Default cRef := ""

    If Empty(cRef)
        MsgStop(STR0026,STR0002)                                        //Voc� est� tentando pegar arquivos de um erro que n�o tem arquivos ? Voc� � muito esperto(a)...
        Return .F.
    Endif

    cRef := Alltrim(cRef)                                               //Grava valor sem espa�os

    aFile := StrTokArr(Alltrim(cRef),"\")                               //Separa o noem do arquivo do diret�rio

    If !File(GetSrvProfString ("ROOTPATH","") + cRef)                   //Verifica se o arquivo existe
        MsgStop(STR0027 + cRef + CRLF + STR0028, STR0002)               //N�o encontrado o arquivo
        Return .F.
    Endif

    If !CpyS2T(cRef,GetTempPath())                                      //Copia arquivo
        MsgStop(STR0029, STR0002)                                       //Eu tentei, mas tem alguma coisa me impedindo de pegar o arquivo do servidor
        Return .F.
    Else
        MsgInfo(STR0030 + aFile[Len(aFile)] +  STR0031,STR0032)         // Arquivo '###' copiado para a pasta temp
    Endif

Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} ValidDate
Valida se alguma funcionalidade est� desatualizada

@type       Method

@author     Jo�o Pedro
@since      15/11/2019
@version    P12

@param      dData    {Date}, Data a ser validada
@param      cFase    {String}, Fase do arquivo (Lib, fonte, etc..)

@return     Self
/*/
//-------------------------------------------------------------------
Method ValidDate(dData, cFase) Class AutoTuco

    Private nTime := 6                              //Tempo em meses que o arquivo pode estar desatualizado
    Private nDiferenca                              //Diferen�a entre o dData e o nTime

    nDiferenca := DateDiffMonth(dData, dDataBase)   //Verifica em meses a diferen�a

    /* Ponto de entrada para modificar a diferen�a e tempo */
    If ExistBlock("TUCOPE")
        Execblock("TUCOPE",.F.,.F., "DESATUALIZADO")
    Endif

    If nDiferenca > nTime                           //Verifica se os arquivos est�o 6 meses desatualizados
        aAdd(::aAtualizados, {cFase, dData})
    Endif

Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} AddInfo
Adiciona informa��es faltantes no error.log

@type       Method

@author     Jo�o Pedro
@since      13/02/2020
@version    P12

@return     aInfo Array, Informa��es que n�o foram encontradas no error.log
/*/
//-------------------------------------------------------------------
Method AddInfo() Class AutoTuco

    Local aInfo := {} //Contem as funcionalidades que n�o existem no error.log

    Iif(Empty(::cRpo), aAdd(aInfo, "Tipo do ambiente"), "")
    Iif(Empty(::cLangRpo), aAdd(aInfo, "Liguagem do RPO"), "")
    Iif(Empty(::cExt), aAdd(aInfo, "Extens�o"), "")
    Iif(Empty(::cSystem), aAdd(aInfo, "Sistema operacional"), "")
    Iif(Empty(::cLOG_LIB), aAdd(aInfo, "Vers�o da LIB"), "")
    Iif(Empty(::cLOG_SERVER), aAdd(aInfo, "Vers�o do Appserver"), "")
    Iif(Empty(::cLOG_BANC), aAdd(aInfo, "Tipo do banco"), "")
    Iif(Empty(::cDbVersion), aAdd(aInfo, "Vers�o do DbAccess"), "")
    Iif(Empty(::cLOG_REL), aAdd(aInfo, "Release do RPO"), "")
    Iif(Empty(::cLicense), aAdd(aInfo, "Licenciamento"), "")
    Iif(Empty(::cLinha), aAdd(aInfo, "Linha do erro"), "")
    Iif(Empty(::cTrace), aAdd(aInfo, "DbTrace"), "")
    Iif(Empty(::cIxBlog), aAdd(aInfo, "IXBLOG"), "")
    Iif(Empty(::cKillStack), aAdd(aInfo, "KillStack"), "")
    Iif(Empty(::cTraceStack), aAdd(aInfo, "TraceStack"), "")
    Iif(Empty(::cSpecKey), aAdd(aInfo, "Special Key"), "")
    Iif(Empty(::cLogProf), aAdd(aInfo, "LogProfiler"), "")
    Iif(Empty(::cTopMemo), aAdd(aInfo, "TopMemoMega"), "")
    Iif(Empty(::cTimeOut), aAdd(aInfo, "TimeOut de conex�o"), "")
    Iif(Empty(::cConsole), aAdd(aInfo, "Console Log"), "")
    Iif(Empty(::cDirCon), aAdd(aInfo, "Diret�rio do arquivo de console"), "")
    Iif(Empty(::nMaxQry), aAdd(aInfo, "Tamanho m�ximo de query"), "")
    Iif(Empty(::nMaxStr), aAdd(aInfo, "Tamanho m�ximo de String"), "")
    Iif(Empty(::cUser), aAdd(aInfo, "Usu�rio que simulou o erro"), "")
    Iif(Empty(::cUserId), aAdd(aInfo, "ID do usu�rio que simulou o erro"), "")
    Iif(Empty(::cDriver), aAdd(aInfo, "Driver de impress�o"), "")
    Iif(Empty(::cDirDrv), aAdd(aInfo, "Diret�rio do driver de impress�o"), "")
    Iif(Empty(::cRdd), aAdd(aInfo, "RDD do ambiente"), "")
    Iif(Empty(::cSimb1), aAdd(aInfo, "Moeda 1"), "")
    Iif(Empty(::cSimb2), aAdd(aInfo, "Moeda 2"), "")
    Iif(Empty(::cSimb3), aAdd(aInfo, "Moeda 3"), "")
    Iif(Empty(::cSimb4), aAdd(aInfo, "Moeda 4"), "")
    Iif(Empty(::cSimb5), aAdd(aInfo, "Moeda 5"), "")
    Iif(Empty(::dDtBase), aAdd(aInfo, "Data base do sistema na gera��o do erro"), "")
    Iif(Empty(::cIp), aAdd(aInfo, "Ip do servidor"), "")
    Iif(Empty(::nPort), aAdd(aInfo, "Porta do servidor"), "")
    Iif(Empty(::cEmpresa), aAdd(aInfo, "Empresa logada"), "")

Return aInfo

//-------------------------------------------------------------------
/*/{Protheus.doc} ValidInfo
Apresenta em tela as informa��es faltantes no error.log

@type       Method

@author     Jo�o Pedro
@since      13/02/2020
@version    P12

@return     Self
/*/
//-------------------------------------------------------------------
Method ValidInfo() Class AutoTuco

    Local aInfo := ::AddInfo()
    Local x
    Local cMsg := STR0104 + CRLF + CRLF //FUNCIONALIDADES N�O ENCONTRADAS NO ERROR.LOG

    If !Empty(aInfo)
        For x := 1 To Len(aInfo)
            cMsg += aInfo[x] + CRLF
        Next x
        EECVIEW(cMsg, STR0105)
    Endif

Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} LimpaSujeiras
Limpa as sujeiras do error.log para deixar a base mais visualmente bonita

@type       Method

@author     Jo�o Pedro
@since      13/02/2020
@version    P12

@return     Self
/*/
//-------------------------------------------------------------------
Method LimpaSujeiras() Class AutoTuco

    /* Necess�rio para quando der erro diretamente no banco Oracle */
    If "ORA-" $ Upper(::cErro)
        ::cErro := Alltrim(StrTokArr(StrTokArr(Alltrim(StrTokArr2(Upper(::cErro), "ORA-")[2]), ":")[2], "(")[1])
    Endif

    /* Necess�rio para quando der erro diretamente no banco Sql Server */
    If "[SQL SERVER]" $ Upper(::cErro)
        ::cErro := Alltrim(StrTokArr(Alltrim(StrTokArr2(Upper(::cErro), "[SQL SERVER]")[2]), "(")[1])
        If "##" $ ::cErro
            ::cErro := Alltrim(StrTokArr(::cErro, ";")[1])
        Endif
    Endif

    /* Retira arquivo tempor�rio do erro */
    If "DBO#" $ Upper(::cErro)
        ::cErro := Alltrim(StrTokArr2(Upper(::cErro), "DBO#")[1])
        If Right(::cErro, 1) == "-"
            ::cErro := Left(::cErro, Len(::cErro) - 1)
        Endif
    Endif

    /* Retira informa��es de valores do processo do cliente */
    If "Value:" $ ::cErro
        ::cErro := Alltrim(StrTokArr2(::cErro, "Value")[1])
    Endif

    /* Necess�rio para quando erro for apresentado em arquivos fisicos */
    If "on file" $ ::cErro
        ::cErro := Alltrim(StrTokArr2(::cErro, "on file")[1])
        If "_SC" $ Upper(::cErro)
            ::cErro := Alltrim(StrTokArr2(::cErro, "_SC")[1])
            If Right(::cErro, 1) == "-"
                ::cErro := Alltrim(Left(::cErro, Len(::cErro) - 1))
            Endif
        Endif
    Endif

    If "in file" $ ::cErro
        ::cErro := Alltrim(StrTokArr2(::cErro, "on file")[1])
        If "_SC" $ Upper(::cErro)
            ::cErro := Alltrim(StrTokArr2(::cErro, "_SC")[1])
            If Right(::cErro, 1) == "-"
                ::cErro := Alltrim(Left(::cErro, Len(::cErro) - 1))
            Endif
        Endif
    Endif

    /* Necess�rio para quando trazer o numero IP do servidor no erro */
    If "\\" $ Left(Alltrim(::cErro), 2) .AND. "CTREE ERROR" $ Upper(::cErro)
        ::cErro := Alltrim(StrTokArr2(Alltrim(StrTokArr2(Upper(::cErro), "CTREE ERROR")[2]), "\\")[1])
    Endif

    If "##TMP" $ ::cErro
        ::cErro := Alltrim(StrokTokArr(StrTokArr(Alltrim(StrtokArr2(::cErro, "##TMP")[2]), ")")[1], "(")[1])
    Endif

    If "_SC" $ Left(::cErro, 2)
        ::cErro := Substr(Alltrim(StrTokArr2(::cErro, "_SC")[1]), 3, Len(::cErro) - 2)
    Endif

    If "C:" $ Left(Upper(::cErro), 2) .OR. "F:" $ Left(Upper(::cErro), 2) .OR. "D:" $ Left(Upper(::cErro), 2)
        ::cErro := Alltrim(StrTokArr(::cErro, ":")[2])
    Endif

    If "NT64" $ Upper(::cErro)
        ::cErro := Alltrim(StrTran(StrTokArr2(::cErro, "NT64")[2], "]", ""))
        If "SQLSTATE" $ Upper(::cErro)
            ::cErro := Alltrim(StrTokArr2(Upper(::cErro), "SQLSTATE")[1])
        Endif
    Endif

Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} PegaRecno
Retorna o Recno do erro corrente

@type       Method

@author     Jo�o Pedro
@since      18/02/2020
@version    P12

@param      cErro    {cErro} String, Erro completo para utiliza��o em query

@return     TMP->RECNO Numeric, Numero do Recno do erro corrente
/*/
//-------------------------------------------------------------------
Method PegaRecno(cErro) Class AutoTuco

    Local cQuery := ""

    cQuery += "SELECT LOG.R_E_C_N_O_ AS RECNO "
    cQuery += "FROM " + RetSQLName("LOG") + " LOG "
    cQuery += "WHERE LOG.LOG_ERRO = '" + Alltrim(cErro) + "'"

    cQuery := ChangeQuery(cQuery)
    If Select("TMP") > 0
        TMP->(DbCloseArea())
    Endif
    DbUseArea(.T., "TOPCONN", TcGenQry(,,cQuery), "TMP", .F., .T.)

Return TMP->RECNO

//-------------------------------------------------------------------
/*/{Protheus.doc} GravaZZZ
Grava a tabela de contato do cliente

@type       Method

@author     Jo�o Pedro
@since      19/02/2020
@version    P12

@return     Self
/*/
//-------------------------------------------------------------------
Method GravaZZZ() Class AutoTuco

    DbSelectArea("ZZZ")

    If ZZZ->(RecLock("ZZZ", .T.))
        ZZZ->ZZZ_ERRO := Alltrim(::cErro)
        If !Empty(::cEmpresa)
            ZZZ->ZZZ_EMP := Alltrim(::cEmpresa)
        Endif
        If !Empty(::cUser)
            ZZZ->ZZZ_NOME := Alltrim(StrTran(::cUser, ".", " "))
        Endif
        ZZZ->(MsUnlock())
    Endif

Return Self