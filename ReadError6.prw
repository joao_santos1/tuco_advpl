/* PME DESCONTINUADO

#Include 'Protheus.ch'
#Include 'Totvs.ch'

User Function xReadSL()

	Local aParamBox := {}
	Local lChamou := .F.

	Private aRet := {} //Variavel necess�ria para n�o dar error.log
	Private nNovo := 0
	Private lCustomizado := .F.
	Private lErro := .F.
	Private cErro := Space(200)

	/* Montagem da tela de par�metro para sele��o do arquivo de erro 
	AADD(aParamBox,{6,"Arquivo ?",Space(70),"","","",70,.T.,"Todos os arquivo (*.*)|*.*"})

	If ParamBox(aParamBox,"Selecione o arquivo",aRet)
		cFile := aRet[1]
		If "LOG_" $ Upper(cFile)
			Processa({||ReadIntSL(cFile)},"Aguarde...","Lendo arquivo de texto...",.F.) //Fun��o que ir� ler linha por linha do error.log
			lChamou := .T.
		Elseif ".log" $ cFile .OR. ".txt" $ cFile
			Processa({||ReadArqSL(cFile)},"Aguarde...","Lendo arquivo de texto...",.F.)
			lChamou := .T.
		Else
			MsgInfo("N�o � poss�vel ler arquivos com esta extens�o","Aten��o")
			Return .F.
		Endif
	Endif

	If lCustomizado
		MsgInfo("H� uma customiza��o na pilha de chamada deste erro","Aten��o")
	Endif

	If !lErro .AND. lChamou
		If MsgYesNo("Deseja excluir o arquivo de erro ?","Aten��o")
			FErase(cFile)
		Endif
	Endif

Return

Static Function ReadArqSL(cFile)

	Local oFile //Objeto que conter� o FWFileReader
	Local cLinha //Guarda a linha atual do arquivo
	Local lAdd := .F.
	Local aErro := {}

	ProcRegua(1000) //Regua de progresso

	oFile := FWFileReader():New(cFile) //Instancia a classe FWFileReader
	If oFile:Open() //Abre o arquivo para leitura
		Do While oFile:hasLine() //Enquanto houver linha no arquivo
			IncProc() //Incrementa regua de progresso
			cLinha := oFile:GetLine() //Pega a linha atual
			If "Caused" $ cLinha .AND. aScan(aErro,"Caused") > 0
				aErro := {}
			Endif
			If "Caused" $ cLinha .OR. lAdd
				lAdd := .T.
				AADD(aErro,cLinha)
			Endif
		Enddo
		Processa({||CutError(aErro)},"Aguarde...","Separando dados do arquivo...",.F.)
		oFile:Close()
	Endif

Return

Static Function CutError(aErro)

	Local aAux := {}
	Local x

	Private cErro := ""
	Private cFonte := ""
	Private cFunc := ""
	Private cPilha := ""
	
	cErro := Alltrim(StrTokArr(aErro[1],":")[2])
	aAux := StrTokArr(aErro[2],"(")
	cFonte := Alltrim(StrTokArr(aAux[2],":")[1])
	cFunc := Alltrim(StrTokArr(aAux[1],".")[Len(StrTokArr(aAux[1],"."))])
	For x := 1 To Len(aErro)
		cPilha += aErro[x] + CRLF
	Next x
	
	Processa({||GrvLSL()},"Aguarde...","Gravando dados na tabela...",.F.)
	
Return

Static Function GrvLSL()

	Local lOk := .F.
	Local nSpaceErro := Len(cErro)
	
	If Len(cErro) < 200
		While nSpaceErro != 200
			nSpaceErro ++
		End
	Endif
	
	nSpaceErro -= Len(cErro)
	
	If Len(cErro) != nSpaceErro
		cErro += Space(nSpaceErro)
	Endif

	ProcRegua(RecCount())
	LSL->(DbGoTop())
	LSL->(DbSetOrder(1))
	
	Do While LSL->(!Eof())
		If LSL->(DbSeek(xFilial()+cErro+cFonte))
			lOk := .T.
			If !Empty(LSL->LSL_SOL) .OR. !Empty(LSL->LSL_REFARQ) //Se j� existir uma solu�ao
				Iif(!Empty(LSL->LSL_SOL),EECVIEW(LSL->LSL_SOL,"Solucao"),EECVIEW(LSL->LSL_SOL,"Referencia Arquivo")) //Se existir solu��o. apresenta ela na tela
				If MsgYesNo("Deseja adicionar uma nova solu��o para este erro ?", "Aten��o")
					CriaSolucao()
					Return .T.
				Else
					Return .F.
				Endif
			Else
				If MsgYesNo("Deseja adicionar solu��o para este erro ?","Tem Solucao")
					CriaSolucao()
					Return .T.
				Else
					Return .F.
				Endif
			Endif
		Endif
		LSL->(DbSkip())
	Enddo
	
	If LSL->(DbSeek(xFilial()+cErro))
		If !Empty(LSL->LSL_SOL) .OR. !Empty(LSL->LSL_REFARQ)
			Iif(!Empty(LSL->LSL_SOL),EECVIEW(LSL->LSL_SOL,"Solucao"),EECVIEW(LSL->LSL_SOL,"Referencia Arquivo")) //Se existir solu��o. apresenta ela na tela
			If MsgYesNo("Essa solu��o foi �til para o erro ?")
				lOk := .T.
				If RecLock("LSL",.T.) //Grava o erro na tabela LOG
					LSL->LSL_FILIAL := xFilial()
					LSL->LSL_ERRO := cErro
					LSL->LSL_FUNC := cFunc
					LSL->LSL_FONTE := cFonte
					LSL->LSL_PILHA := cPilha
					LSL->(MsUnlock())
				Endif
			Endif
		Endif
	Endif
	
	If !lOk
		If MsgYesNo("Erro desconhecido, deseja adicionar a base de dados ?")
			If RecLock("LSL",.T.) //Grava o erro na tabela LOG
				LSL->LSL_FILIAL := xFilial()
				LSL->LSL_ERRO := cErro
				LSL->LSL_FUNC := cFunc
				LSL->LSL_FONTE := cFonte
				LSL->LSL_PILHA := cPilha
				LSL->(MsUnlock())
			Endif
		Else
			Return .F.
		Endif
	Endif

Return

Static Function CriaSolucao()

	Local cSol := Alltrim(LSL->LSL_SOL) //Variavel que ir� guardar a LOG_SOL
	Local cLabel := ""
	Local oMemo
	Local oFont := TFont():New("Courier New",09,15)
	Local bOk      := {|| nOpc := 1,oDlg:End()}
	Local bCancel  := {|| oDlg:End()}
	Local aButtons := {}
	Local nOpc := 0

	AADD(aButtons,{"NOTE",{|| ExecNote(LSL->LSL_SOL,"Solucao.txt")},"Notepad"}) //Exporta a solu��o para .txt
	AADD(aButtons,{"FILES",{|| BuscaArq(LSL->LSL_REFARQ)},"Anexos"}) //Busca os arquivos gravados para este erro

	/* Cria��o da tela de solu��o 
	DEFINE MSDIALOG oDlg TITLE "Inclus�o Solu��o" FROM 9,0 TO 39,85 OF oDlg

	oPanel := TPanel():New(0,0,"",oDlg,,.F.,.F.,,,90,165)
	oPanel:Align := CONTROL_ALIGN_ALLCLIENT

	@ 05,05 TO 190,330 Label cLabel PIXEL OF oPanel
	@ 15,10 GET oMemo Var cSol MEMO HSCROLL FONT oFont SIZE 315,169 OF oPanel PIXEL

	oMemo:lWordWrap := .T.
	oMemo:EnableVScroll(.T.)
	oMemo:EnableHScroll(.T.)

	ACTIVATE MSDIALOG oDlg ON INIT EnchoiceBar(oDlg,bOk,bCancel,,aButtons) CENTERED

	If nOpc == 1
		If !Empty(cSol)
			If RecLock("LSL",.F.)
				LSL->LSL_SOL := Alltrim(cSol) //Grava a solu��o na tabela
			Endif
		Endif
	Else
		If !MsgYesNo("Solucao n�o informada, deseja prosseguir ?","Atencao")
			CriaSolucao()
		Else
			U_ReadError()
		Endif
	Endif

Return Nil

Static Function ReadIntSL(cFile)

	Local oFile //Objeto que conter� o FWFileReader
	Local cLinha //Guarda a linha atual do arquivo
	Local aErro := {}
	Local lRoda := .F.

	ProcRegua(1000) //Regua de progresso

	oFile := FWFileReader():New(cFile) //Instancia a classe FWFileReader
	If oFile:Open() //Abre o arquivo para leitura
		Do While oFile:hasLine() //Enquanto houver linha no arquivo
			IncProc() //Incrementa regua de progresso
			cLinha := oFile:GetLine() //Pega a linha atual
			If !"ERROR" $ cLinha .OR. !"MESSAGE" $ cLinha
				lRoda := .T.
			Else
				lRoda := .F.
			Endif
			If "ERROR" $ cLinha .OR. lRoda
				AADD(aErro,cLinha)
			Endif
		Enddo
		Processa({||CutInt(aErro)},"Aguarde...","Separando dados do arquivo...",.F.)
		oFile:Close()
	Endif

Return

Static Function CutInt(aErro)

	Local aErros := {}
	Local aData := {}
	Local aHoras := {}
	Local cMsg := ""
	Local y, x, z, q
	Local oPrc, oQual
	Local cVar := "		"
	Local nOpca := 1
	Local oDlg
	Local aAux := {}
	Local cAux := ""
	Local nLoop := 0
	
	For q := 1 To Len(aErro)
		If "ERROR" $ aErro[q]
			If Len(aErro) <> q
				If "ERROR" $ aErro[q+1]
					AADD(aAux,aErro[q])
				Endif
			Endif
			If !Empty(cAux)
				AADD(aAux,cAux)
			Endif
			cAux := ""
			nLoop := 0
		Endif
		If !"ERROR" $ aErro[q] .AND. !"MESSAGE" $ aErro[q]
			If nLoop == 0
				cAux := aErro[q - 1] + " " + aErro[q]
			Else
				cAux += " " + aErro[q]
			Endif
			nLoop := 1
		Endif
	Next q
	
	For y := 1 To Len(aAux)
		AADD(aErros,Alltrim(StrTokArr(aAux[y],";")[3]))
		AADD(aData,Alltrim(Left(StrTokArr(aAux[y],";")[2],11)))
		AADD(aHoras,Alltrim(Right(StrTokArr(aAux[y],";")[2],9)))
	Next y
	
	If Len(aErros) >= 3
		cMsg += "				-------------�ltimos Erros-------------" + CRLF + CRLF
		cMsg += aErros[Len(aErros)] + CRLF + "---- " + aData[Len(aData)] + " - " + aHoras[Len(aHoras)] + CRLF + CRLF
		cMsg += aErros[Len(aErros) - 1] + CRLF + "---- " + aData[Len(aData) - 1] + " - " + aHoras[Len(aHoras) - 1] + CRLF + CRLF
		cMsg += aErros[Len(aErros) - 2] + CRLF + "---- " + aData[Len(aData) - 2] + " - " + aHoras[Len(aHoras) - 2] + CRLF + CRLF
		cMsg += "				-------------Demais Erros-------------" + CRLF + CRLF
		For x := 1 To Len(aErros)
			cMsg += aErros[x] + CRLF + "---- " + aData[x] + " - " + aHoras[x] + CRLF + CRLF
		Next x
	Else
		cMsg += "				-------------�ltimos Erros-------------" + CRLF + CRLF
		For x := 1 To Len(aErros)
			cMsg += aErros[x] + CRLF + "---- " + aData[x] + " - " + aHoras[x] + CRLF + CRLF
		Next x
	Endif
	
	EECVIEW(cMsg,"Erros")
	
	For z := 1 To Len(aErros)
		If LSL->(DbSeek(xFilial("LSL")+aErros[z]))
			aDel(aErros,z)
		Endif
	Next z
	
	DEFINE MSDIALOG oDlg TITLE "Gravar Erros ?" From 290,140 To 800,1000 OF oMainWnd PIXEL
		@ 10,20 TO 240,410 LABEL "" OF oDlg  PIXEL
		@ 10,20 LISTBOX oQual VAR cVar Fields HEADER "",OemToAnsi("Grava��o"),"Gravacao"  SIZE 400,230 ON DBLCLICK (AddErro(oQual:nAt),oQual:Refresh()) NOSCROLL OF oDlg PIXEL
		oQual:SetArray(aErros)
		DEFINE SBUTTON FROM 242,030 TYPE 1 Action (nOpca:=1,oDlg:End()) ENABLE OF oDlg PIXEL
		DEFINE SBUTTON FROM 242,084 TYPE 3 Action (nOpca:=2,oDlg:End()) ENABLE OF oDlg PIXEL
		DEFINE SBUTTON FROM 242,138 TYPE 2 ACTION oDlg:End() ENABLE OF oDlg PIXEL
	ACTIVATE MSDIALOG oDlg 

Return
*/