#Include 'Protheus.ch'
#Include 'Totvs.ch'
#Include 'Tuco.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} Anexo
Classe principal para anexar arquivos a erros

@type       Class

@author     Jo�o Pedro
@since      13/12/2018
@version    P12
/*/
//-------------------------------------------------------------------
Class Anexo From Tuco

    Data aRet as Array //Var�avel necess�ria para n�o causar error.log ao entrar na rotina

    Method New() Constructor
    Method paramAnexo()         //Abre window para selecionar arquivo
    Method VerPasta(cFile)      //Cria pasta e zipa arquivo no servidor
    Method limpaAnexo()         //Limpa todos os anexos do erro no servidor

End Class

//-------------------------------------------------------------------
/*/{Protheus.doc} New
Instancia var�iveis necess�rias para o funcionamento da rotina

@type       Method

@author     Jo�o Pedro
@since      13/12/2018
@version    P12

@return     Self
/*/
//-------------------------------------------------------------------
Method New() Class Anexo

    ::aRet  := {}

Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} paramAnexo
Abre janela para sele��o de arquivo da DU-E

@type       Method

@author     Jo�o Pedro
@since      13/12/2018
@version    P12

@return     Self
/*/
//-------------------------------------------------------------------
Method paramAnexo() Class Anexo

    Local cFile                                                                     //Nome do arquivo a ser analisado
    Local oAuto         := AutoTuco():New()                                         //Instancia a classe autoTuco para fazer o download do arquivo
    Local oDlg
    Local cDirArq       := Space(100)                                               //Diret�rio do arquivo
    Local bDir          := {|| cDirArq := cGetFile("","Arquivos",1,"C:\",.F.,)}     //Bloco que ir� abrir tela para sele��o do arquivo
    Local bAction       := {|| oDlg:End() }                                         //A��o a ser executada quando clicar em Ok
    Local nLin          := 25                                                       //Posi��o da linha em Pixels dos dados dentro da janela
    Local nCol          := 12                                                       //Posi��o da coluna em Pixels dos dados dentro da janela
    Local lCancel       := .F.                                                      //Define se o usu�rio cancelou a rotina sem informar nenhum arquivo
    Local bCancel       := {|| lCancel := .T., oDlg:End()}                          //Bloco a ser executado quando cancelar a tela
    Local oModel2 
    Local oModel3
    Local oView1

    /* Busca o arquivo de conting�ncia */
    If !Empty(LOG->LOG_REFCON)
        If MsgYesNo(STR0111,STR0002)                                                //"J� existe um arquivo de conting�ncia para este erro no servidor. Deseja realizar o download dele ?"
            oAuto:BuscaArq(LOG->LOG_REFCON)                                         //Faz o download do arquivo para a m�quina do usu�rio
            Return .T.
        Endif
    Endif

    /* Busca o arquivo principal */
    If !Empty(LOG->LOG_REFARQ)
        If MsgYesNo(STR0046,STR0002)                                                //J� tem alguma coisa para este erro no servidor, talvez seja um v�rus ou algum arquivo. Posso ir la pegar ele ?
            oAuto:BuscaArq(LOG->LOG_REFARQ)                                         //Faz o download do arquivo para o Desktop
            Return .T.
        Endif
    Endif

    DEFINE MSDIALOG oDlg TITLE "Escolha o arquivo" FROM 320, 400 TO 570, 720 OF oMainWnd PIXEL

        oPanel:= TPanel():New(0, 0, "", oDlg,, .F., .F.,,, 90, 165)                 //Cria Painel principal
        oPanel:Align:= CONTROL_ALIGN_ALLCLIENT                                      //Define alinhamento do painel

        @ nLin, nCol Say "Arquivo ?" Size 160, 08 PIXEL OF oPanel
        nLin += 10
        @ nLin, nCol MsGet cDirArq Size 120, 08 PIXEL OF oPanel
        @ nLin, nCol+120 BUTTON "..." ACTION Eval(bDir) Size 10, 10 PIXEL OF oPanel

    ACTIVATE MSDIALOG oDlg ON INIT EnchoiceBar(oDlg, bAction, bCancel) CENTERED

    If !Empty(cDirArq)
        aAdd(::aRet, "\" + StrTokArr(cDirArq, "\")[Len(StrTokArr(cDirArq, "\"))])   //Adiciona o nome do arquivo principal
        cFile := ::aRet[1]
        If !":" $ Upper(cDirArq)                                                    //N�o anexa arquivos de outros servidores
            MsgAlert(STR0047,STR0002)                                               //Da muito trabalho buscar arquivos em outros computadores...Ent�o informe um diret�rio local
            U_Anexo()                                                               //Chama a rotina novamente
        Endif
        Begin Transaction
            Processa({||::VerPasta(cDirArq)},STR0006,STR0048,.F.)                       //Fun��o que ir� ler linha por linha do erro
        End Transaction
    Elseif !lCancel
        MsgStop(STR0077, STR0002)                                                   //Voc� n�o informa nenhum arquivo e quer que eu processe o que ?
    Endif

    oModel2 := FwModelActive()
    oModel3 := oModel2:GetModel('LOGF')
    oModel3:LoadValue("LOG_REFARQ", LOG->LOG_REFARQ)
    oModel3:LoadValue("LOG_REFCON", LOG->LOG_REFCON)

    oView1 := FWViewActive()
    oView1:Refresh()

Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} VerPasta
Zipa os arquivos anexados

@type       Method

@author     Jo�o Pedro
@since      13/12/2018
@version    P12

@param      cFile {String}, diret�rio onde se encontra o arquivo a ser anexado

@return     Self
/*/
//-------------------------------------------------------------------
Method VerPasta(cFile) Class Anexo

    Local cDirStart :=GetSrvProfString("STARTPATH","")  //Define a pasta a ser anexada como a System
    Local lRet                                          //Define se foi poss�vel criar uma nova pasta no servidor
    Local nRet                                          //Define se conseguiu excluir arquivo do servidor
    Local cPasta    := ""                               //Nome da pasta a ser criada no servidor
    Local aArq      := {}                               //Nome do arquivo anexado
    Local aFiles    := {}                               //Lista de arquivos que ir�o ser anexados
    Local lBreak    := .T.                              //Variavel para parar o loop de contador do nome da pasta
    Local nCount    := 0                                //Contador para o nome da pasta
    Local cDirBase  := ""                               //Diret�rio base daonde est� vindo o erro
    Local x, y, z                                       //Variaveis de Loop
    Local cDirExist := ""                               //Diret�rio se j� existir arquivo de anexo
    Local cDirGrava := ""                               //Diret�rio para zipar o arquivo (System+Contador)
    Local aZipFiles := {}                               //Lista os arquivos que estavam dentro do arquivo .zip j� existente
    Local lCont     := .F.

    ProcRegua(100)

    If MsgYesNo(STR0112, STR0002) //"Este arquivo � um arquivo de conting�ncia ?"
        If !Empty(LOG->LOG_REFARQ) .OR. !Empty(LOG->LOG_SOL)
            MsgStop(STR0113, STR0002) //"N�o � poss�vel incluir uma conting�ncia quando h� solu��o homologada vinculada ao erro"
            Return .F.
        Endif
        lCont := .T.
    Endif

    If !lCont
        If !Empty(LOG->LOG_REFARQ)
            cDirStart := Alltrim(LOG->LOG_REFARQ)           //Pega o diret�rio do campo se j� existir anexos
        Else
            While lBreak
                If nCount == 0                              //Para conseguir separar o nome da pasta qunado n�o houver outra referencia na base
                    nCount ++
                    Loop
                Endif
                If File(cDirStart + cValToChar(nCount))     //Verifica se a pasta j� existe na System
                    nCount ++
                Else
                    lBreak := .F.
                Endif
            Enddo
            cPasta := cDirStart + cValToChar(nCount)        //Grava o System+Contador
            lRet := MakeDir(cPasta)                         //Cria a pasta na System
            If lRet != 0
                MsgStop(STR0049 + Str(FError()) ,STR0002)   //Esse servidor me odeia :-(. Erro:
                Return .F.
            Endif
        Endif
    Else
        If !Empty(LOG->LOG_REFCON)
            cDirStart := Alltrim(LOG->LOG_REFCON)           //Pega o diret�rio do campo se j� existir anexos
        Else
            While lBreak
                If nCount == 0                              //Para conseguir separar o nome da pasta qunado n�o houver outra referencia na base
                    nCount ++
                    Loop
                Endif
                If File(cDirStart + cValToChar(nCount) + "_CONT")     //Verifica se a pasta j� existe na System
                    nCount ++
                Else
                    lBreak := .F.
                Endif
            Enddo
            cPasta := cDirStart + cValToChar(nCount) + "_CONT"        //Grava o System+Contador
            lRet := MakeDir(cPasta)                         //Cria a pasta na System
            If lRet != 0
                MsgStop(STR0049 + Str(FError()) ,STR0002)   //Esse servidor me odeia :-(. Erro:
                Return .F.
            Endif
        Endif
    Endif

    aArq := StrTokArr(cFile,"\")                        //Separa o nome do arquivo do par�metro inicial do m�todo

    For y := 1 To Len(StrTokArr(cDirStart, "\")) - 1
        cDirExist += StrTokArr(cDirStart, "\")[y] + Iif(y == Len(StrTokArr(cDirStart, "\")) - 1, "", "\")                       //Separa o diret�rio do nome do arquivo quando j� existir refer�ncias no servidor
    Next y

    If !lCont
        If !File(Iif(!Empty(LOG->LOG_REFARQ), cDirExist, Iif(Empty(cPasta),cDirStart,cPasta)) + "\" + Alltrim(aArq[Len(aArq)]) )    //Verifica se o arquivo j� existe no servidor
            If !CpyT2S(cFile,Iif(!Empty(LOG->LOG_REFARQ), cDirExist,Iif(Empty(cPasta),cDirStart,cPasta)) + "\")                     //Copia o arquivo da area local para a remota
                IncProc()
                MsgStop(STR0050, STR0002)                   //N�o foi poss�vel copiar arquivo para o servidor
                If Empty(LOG->LOG_REFARQ)
                    DirRemove(cPasta)                       //Exclui a pasta se houver erros
                Endif
                Return .F.
            Endif
        Endif
    Else
        If !File(Iif(!Empty(LOG->LOG_REFCON), cDirExist, Iif(Empty(cPasta),cDirStart,cPasta)) + "\" + Alltrim(aArq[Len(aArq)]) )    //Verifica se o arquivo j� existe no servidor
            If !CpyT2S(cFile,Iif(!Empty(LOG->LOG_REFCON), cDirExist,Iif(Empty(cPasta),cDirStart,cPasta)) + "\")                     //Copia o arquivo da area local para a remota
                IncProc()
                MsgStop(STR0050, STR0002)                   //N�o foi poss�vel copiar arquivo para o servidor
                If Empty(LOG->LOG_REFCON)
                    DirRemove(cPasta)                       //Exclui a pasta se houver erros
                Endif
                Return .F.
            Endif
        Endif
    Endif

    For x := 1 To Len(aArq) - 1
        cDirBase += aArq[x] + "\"                       //Separa o arquivo do diret�rio base
    Next x

    If !lCont
        AADD(aFiles, Iif(!Empty(LOG->LOG_REFARQ), cDirExist,Iif(Empty(cPasta),cDirStart,cPasta)) + "\" + Alltrim(aArq[Len(aArq)])) //Adiciona os arquivos a serem zipados na variavel aFiles
    Else
        AADD(aFiles, Iif(!Empty(LOG->LOG_REFCON), cDirExist,Iif(Empty(cPasta),cDirStart,cPasta)) + "\" + Alltrim(aArq[Len(aArq)]))
    Endif

    /* Realizar a separa��o dos nomes das pastas para nao zipar elas junto com o arquivo */
    If nCount == 0
        For z := 1 To Len(StrTokArr(cDirStart, "\"))
            If z != Len(StrTokArr(cDirStart, "\"))
                cDirGrava += StrTokArr(cDirStart, "\")[z] + "\"
            Endif
        Next z
    Else
        For z := 1 To Len(StrTokArr(aFiles[1], "\"))
            If z != Len(StrTokArr(aFiles[1], "\"))
                cDirGrava += StrTokArr(aFiles[1], "\")[z] + "\"
            Endif
        Next z
    Endif

    If !lCont
        If !".zip" $ cFile
            If !Empty(LOG->LOG_REFARQ) .AND. FUnzip(cDirStart, cDirGrava) != 0      //S� tenta dezipar se j� tiver arquivo no servidor
                MsgStop(STR0079, STR0002)                                           //Chama o Winrar porque eu n�o estou dando conta de dezipar isso n�o
                If Empty(LOG->LOG_REFARQ)
                    DirRemove(cPasta)                                               //Remove a pasta se der erro
                Endif
                Return .F.
            Else
                If !Empty(LOG->LOG_REFARQ) .AND. FErase(cDirExist + "\" + StrTokArr(cDirExist, "\")[Len(StrTokArr(cDirExist, "\"))] + ".zip") == -1     //Exclui o arquivo .zip antigo
                    MsgStop(STR0080, STR0002)                                      //N�o estou conseguindo excluir o arquivo .zip antigo
                    Return .F.
                Endif
            Endif
            If !Empty(LOG->LOG_REFARQ)
                aZipFiles := Directory(cDirGrava + "*.*",,,.F.)                     //Pega todos os arquivos que estavam .zip para zipar junto com o novo arquivo
                aFiles := {}                                                        //Limpa o aFiles para adicionar os arquivos que estavam no .zip
                For x := 1 To Len(aZipFiles)
                    aAdd(aFiles, cDirGrava + aZipFiles[x][1])                       //Adiciona todos os arquivos no aFiles
                Next x
            Endif
            If FZip(Iif(!Empty(LOG->LOG_REFARQ), cDirExist + "\" + StrTokArr(cDirExist, "\")[Len(StrTokArr(cDirExist, "\"))] + ".zip",Iif(Empty(cPasta),cDirStart,cPasta) + "\"+ cValToChar(nCount) + ".zip"), aFiles, cDirGrava) != 0 //Zipa o arquivo antes de salvar no servidor
                IncProc()
                MsgStop(STR0051,STR0002)                                            //Imposs�vel zipar este arquivo, ou eu sou muito burro...
                If Empty(LOG->LOG_REFARQ)
                    DirRemove(cPasta)                                               //Remove a pasta se der algum erro
                Endif
                Return .F.
            Endif
        Endif
    Else
        If !".zip" $ cFile
            If !Empty(LOG->LOG_REFCON) .AND. FUnzip(cDirStart, cDirGrava) != 0      //S� tenta dezipar se j� tiver arquivo no servidor
                MsgStop(STR0079, STR0002)                                           //Chama o Winrar porque eu n�o estou dando conta de dezipar isso n�o
                If Empty(LOG->LOG_REFCON)
                    DirRemove(cPasta)                                               //Remove a pasta se der erro
                Endif
                Return .F.
            Else
                If !Empty(LOG->LOG_REFCON) .AND. FErase(cDirExist + "\" + Alltrim(StrTokArr(StrTokArr(cDirExist, "\")[Len(StrTokArr(cDirExist, "\"))],"_")[1]) + "_CONT" + ".zip") == -1
                    MsgStop(STR0080, STR0002)                                      //N�o estou conseguindo excluir o arquivo .zip antigo
                    Return .F.
                Endif
            Endif
            If !Empty(LOG->LOG_REFCON)
                aZipFiles := Directory(cDirGrava + "*.*",,,.F.)                     //Pega todos os arquivos que estavam .zip para zipar junto com o novo arquivo
                aFiles := {}                                                        //Limpa o aFiles para adicionar os arquivos que estavam no .zip
                For x := 1 To Len(aZipFiles)
                    aAdd(aFiles, cDirGrava + aZipFiles[x][1])                       //Adiciona todos os arquivos no aFiles
                Next x
            Endif
            If FZip(Iif(!Empty(LOG->LOG_REFCON), cDirExist + "\" + StrTokArr(cDirExist, "\")[Len(StrTokArr(cDirExist, "\"))] + ".zip",Iif(Empty(cPasta),cDirStart,cPasta) + "\"+ cValToChar(nCount) + "_CONT" + ".zip"), aFiles, cDirGrava) != 0 //Zipa o arquivo antes de salvar no servidor
                IncProc()
                MsgStop(STR0051,STR0002)                                            //Imposs�vel zipar este arquivo, ou eu sou muito burro...
                If Empty(LOG->LOG_REFCON)
                    DirRemove(cPasta)                                               //Remove a pasta se der algum erro
                Endif
                Return .F.
            Endif
        Endif
    Endif

    RecLock("LOG",.F.)
    If !lCont
        LOG->LOG_REFARQ := Alltrim(Iif(Empty(cPasta),Left(cDirGrava, Len(cDirGrava) - 1),cPasta) + "\"+ Iif(!".zip" $ cFile,Iif(Empty(cPasta),StrTokArr(cDirGrava, "\")[Len(StrTokArr(cDirGrava, "\"))],cValToChar(nCount)) + ".zip",aArq[Len(aArq)]))
    Else
        LOG->LOG_REFCON := Alltrim(Iif(Empty(cPasta),Left(cDirGrava, Len(cDirGrava) - 1),cPasta) + "\"+ Iif(!".zip" $ cFile,Iif(Empty(cPasta),StrTokArr(cDirGrava, "\")[Len(StrTokArr(cDirGrava, "\"))],cValToChar(nCount) + "_CONT") + ".zip",aArq[Len(aArq)]))
    Endif
    LOG->(MsUnlock())

    /* Para cada arquivo zipado, exclui eles da pasta */
    If !".zip" $ cFile
        For x := 1 To Len(aFiles)
            nRet := FErase(Iif(Empty(cPasta),Left(cDirGrava, Len(cDirGrava) - 1),cPasta) + "\" + StrTokArr(aFiles[x],"\")[Len(StrTokArr(aFiles[x],"\"))]) //Exclui os arquivos copiados para o servidor

            If nRet < 0 
                MsgAlert(STR0052 + Str(FError()),STR0002)                       //N�o foi poss�vel excluir o arquivo
            Endif
        Next x
    Endif

    If MsgYesNo(STR0053, STR0002)                                               //Posso matar este arquivo ?
        FErase(cFile)                                                           //Exclui arquivo da �rea local
    Endif
    
Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} limpaAnexo
Exclui todos os anexos vinculados a este erro do servidor

@type       Method

@author     Jo�o Pedro
@since      13/12/2018
@version    P12

@return     Self
/*/
//-------------------------------------------------------------------
Method limpaAnexo() Class Anexo

    Local oModel2
    Local oModel3
    Local oView1

    If Empty(LOG->LOG_REFARQ) .AND. Empty(LOG->LOG_REFCON)
        MsgStop(STR0093, STR0002)     //N�o h� arquivos anexados a este erro
        Return .F.                                      
    Endif
    If !MsgYesNo(STR0092, STR0002)                                       //Se existir, deseja manter o arquivo de solu��o principal ?
        If !Empty(LOG->LOG_REFARQ)
            FErase(Alltrim(LOG->LOG_REFARQ))
            DirRemove("\system\" + StrTokArr(LOG->LOG_REFARQ, "\")[2])          //Remove a pasta para que n�o deixe "sujeiras"
            RecLock("LOG",.F.)
            LOG->LOG_REFARQ := ""                                               //Grava o campo de referencia como vazio
            LOG->(MsUnlock())
        Endif
        If !Empty(LOG->LOG_REFCON)
            FErase(Alltrim(LOG->LOG_REFCON))
            DirRemove("\system\" + StrTokArr(LOG->LOG_REFCON, "\")[2])          //Remove a pasta para que n�o deixe "sujeiras"
            RecLock("LOG",.F.)
            LOG->LOG_REFCON := ""                                               //Grava o campo de referencia como vazio
            LOG->(MsUnlock())
        Endif
    Else
        If !Empty(LOG->LOG_REFCON)
            FErase(Alltrim(LOG->LOG_REFCON))
            DirRemove("\system\" + StrTokArr(LOG->LOG_REFCON, "\")[2])          //Remove a pasta para que n�o deixe "sujeiras"
            RecLock("LOG",.F.)
            LOG->LOG_REFCON := ""                                               //Grava o campo de referencia como vazio
            LOG->(MsUnlock())
        Endif
    Endif

    /* Realiza o Refresh na tela */
    oModel2 := FwModelActive()
    oModel3 := oModel2:GetModel('LOGF')
    oModel3:LoadValue("LOG_REFARQ", LOG->LOG_REFARQ)
    oModel3:LoadValue("LOG_REFCON", LOG->LOG_REFCON)

    oView1 := FWViewActive()
    oView1:Refresh()

Return Self