#Include 'Protheus.ch'
#Include 'Totvs.ch'
#Include 'Tuco.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} DeOpiniao
Classe principal da sugest�o de melhoria

@type       Class

@author     Jo�o Pedro
@since      16/01/2018
@version    P12
/*/
//-------------------------------------------------------------------
Class DeOpiniao From Tuco

    Data oPanel         as Object
    Data oNewPag        as Object
    Data oStepWiz       as Object
    Data oDlg           as Object
    Data oPanelBkg      as Object
    Data cNome          as String
    Data cOpiniao       as String

    Method New() Constructor
    Method MontaWizard()
    Method cria_pg1(oPanel)
    Method cria_pg2(oPanel)
    Method valida_pg2()
    Method cria_pg3(oPanel)
    Method EnviaMail()

End Class

//-------------------------------------------------------------------
/*/{Protheus.doc} New
Instancia variaveis importantes

@type       Method

@author     Jo�o Pedro
@since      16/01/2018
@version    P12
/*/
//-------------------------------------------------------------------
Method New() Class DeOpiniao

    ::cNome := Space(200)

Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} MontaWizard
Monta Wizard para informar a melhoria

@type       Method

@author     Jo�o Pedro
@since      16/01/2018
@version    P12

@return     Self
/*/
//-------------------------------------------------------------------
Method MontaWizard() Class DeOpiniao

    Local cTitle := "Opini�o"

    If Empty(GetMv("MV_RELSERV")) .OR. Empty(GetMv("MV_RELACNT")) .OR. Empty(GetMv("MV_RELPSW")) .OR. Empty(GetMv("MV_PORSMTP"))

        MsgStop(STR0119, STR0002) //"H� par�metros necess�rios para envio de e-mail n�o preenchidos"

    Else

        DEFINE DIALOG ::oDlg TITLE cTitle PIXEL

            ::oDlg:nWidth   := 800
            ::oDlg:nHeight  := 600
            ::oPanelBkg     := TPanel():New(0,0,"",::oDlg,,,,,,400,287)
            ::oStepWiz      := FWWizardControl():New(::oPanelBkg)
            ::oStepWiz:ActiveUISteps()

            ::oNewPag       := ::oStepWiz:AddStep("1")
            ::oNewPag:SetStepDescription("Bem vindo")
            ::oNewPag:SetConstruction({|Panel|::cria_pg1(Panel)})
            ::oNewPag:SetNextAction({|| .T.})
            ::oNewPag:SetCancelAction({||::oDlg:End()})

            ::oNewPag       := ::oStepWiz:AddStep("2", {|Panel|::cria_pg2(Panel)})
            ::oNewPag:SetStepDescription("Formul�rio")
            ::oNewPag:SetNextAction({||::valida_pg2()})
            ::oNewPag:SetCancelAction({||::oDlg:End()})
            ::oNewPag:SetPrevAction({|| .T.})
            ::oNewPag:SetPrevTitle("Voltar")

            ::oNewPag       := ::oStepWiz:AddStep("3", {|Panel|::cria_pg3(Panel)})
            ::oNewPag:SetStepDescription("Sucesso")
            ::oNewPag:SetNextAction({||::oDlg:End()})
            ::oNewPag:SetCancelAction({||::oDlg:End()})
            ::oNewPag:SetPrevAction({|| .T.})
            ::oNewPag:SetPrevTitle("Voltar")

            ::oStepWiz:Activate()

        ACTIVATE DIALOG ::oDlg CENTERED

        ::oStepWiz:Destroy()
    Endif

Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} cria_pg1
Cria p�gina 1

@type       Method

@author     Jo�o Pedro
@since      16/01/2018
@version    P12

@param      oPanel {Object}, Painel da pagina

@return     Self
/*/
//-------------------------------------------------------------------
Method cria_pg1(oPanel) Class DeOpiniao

    Local oSay
    Local oFont    := TFont():New('Courier new',,-16,.T.)

    oSay := TSay():New(10,10,{|| STR0081 + CRLF + CRLF + CRLF + STR0082 + "." + STR0083 + "." + STR0084 + CRLF + CRLF + CRLF + STR0085},oPanel,,oFont,,,,.T.,,,290,290)

Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} cria_pg2
Cria p�gina 2

@type       Method

@author     Jo�o Pedro
@since      16/01/2018
@version    P12

@param      oPanel {Object}, Painel da pagina

@return     Self
/*/
//-------------------------------------------------------------------
Method cria_pg2(oPanel) Class DeOpiniao

    Local oGetNome, oGetMsg
    Local oSayNome, oSayMsg

    oSayNome := TSay():New(10,10,{||'Nome'},oPanel,,,,,,.T.,,,200,20)
    oGetNome := TGet():New(20,10,{|u| if(PCount() > 0, ::cNome := u, ::cNome)}, oPanel, 096, 009, "@!",,0,,,.F.,,.T.,,.F.,,.F.,.F.,,.F.,.F.,,::cNome,,,, )
    oSayMsg  := TSay():New(35,10,{||'Mensagem'},oPanel,,,,,,.T.,,,200,20)
    oGetMsg  := TMultiGet():New(45,10,{|u|If(PCount() > 0, ::cOpiniao := u, ::cOpiniao)},oPanel,260,92,,,,,,.T. )

Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} valida_pg2
Valida dados da p�gina 2

@type       Method

@author     Jo�o Pedro
@since      16/01/2018
@version    P12

@return     Logical, passou ou n�o {.T., .F.}
/*/
//-------------------------------------------------------------------
Method valida_pg2() Class DeOpiniao

    If Empty(::cNome) .OR. Empty(::cOpiniao)
        Return .F.
    Endif

    If !::EnviaMail()
        Return .F.
    Endif

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} cria_pg3
Cria p�gina 3

@type       Method

@author     Jo�o Pedro
@since      16/01/2018
@version    P12

@param      oPanel {Object}, Painel da pagina

@return     Self
/*/
//-------------------------------------------------------------------
Method cria_pg3(oPanel) Class DeOpiniao

    Local oSay
    Local oFont    := TFont():New('Courier new',,-16,.T.)

    oSay := TSay():New(10,10,{|| STR0086},oPanel,,oFont,,,,.T.,,,290,290) //Muito obrigado pelo seu contato, irei guardar esse recado com todo meu amor <3

Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} EnviaMail
Envia o e-mail com a sugest�o

@type       Method

@author     Jo�o Pedro
@since      16/01/2018
@version    P12

@return     Logical, Enviou ou n�o ou e-mail {lRet}
/*/
//-------------------------------------------------------------------
Method EnviaMail() Class DeOpiniao

    Local oMail
    Local oMessage
    Local lRet              := .F.
    Local nErro
    Local lSSL              := GetMv("MV_RELSSL")
    Local lTLS              := GetMv("MV_RELTLS")
    Local cServidor         := GetMv("MV_RELSERV")
    Local cConta            := GetMv("MV_RELACNT")
    Local cSenha            := GetMv("MV_RELPSW")
    Local nPort             := GetMv("MV_PORSMTP")
    Local nTimeOut          := GetMv("MV_RELTIME")
    Local lAutentica        := GetMv("MV_RELAUTH")
    Local cUsuario          := GetMv("MV_RELAUSR")
    Local cSenhaAut         := GetMv("MV_RELAPSW")
    Local cTo               := "joao.santos@thomsonreuters.com"
    Local cSubject          := "Tuco Message"

    oMail := TMailManager():New()

    If lSSL
        oMail:SetUseSSL(.T.)
    Elseif lTLS
        oMail:SetUseTLS(.T.)
    Endif

    oMail:Init('', cServidor, cConta, cSenha, 0, nPort)
    oMail:SetSmtpTimeOut(nTimeOut)

    nErro := oMail:SmtpConnect()

    If nErro <> 0
        MsgStop(STR0087 + " " + oMail:GetErrorString(nErro), STR0002) //N�o foi poss�vel conectar com o servidor de e-mail
    Else
        If lAutentica
            nErro := oMail:SmtpAuth(cUsuario, cSenhaAut)
            If nErro <> 0
                MsgStop(STR0088 + " " + oMail:GetErrorString(nErro), STR0002) //N�o foi poss�vel autenticar o usu�rio
                nErro := oMail:SMTPDisconnect()
                If nErro <> 0
                    MsgStop(STR0089 + " " + oMail:GetErrorString(nErro), STR0002) //Falha ao desconectar do servidor
                Endif
            Endif
        Endif
        If nErro == 0
            oMessage := TMailMessage():New()
            oMessage:Clear()
            oMessage:cFrom      := cConta
            oMessage:cTo        := cTo
            oMessage:cSubject   := cSubject
            oMessage:cBody      := "De: " + Alltrim(::cNome) + CRLF + CRLF + Alltrim(::cOpiniao)

            nErro := oMessage:Send(oMail)

            If nErro <> 0
                MsgStop(STR0090 + " " + oMail:GetErrorString(nErro), STR0002) //Falha ao enviar e-mail
            Endif

            nErro := oMail:SMTPDisconnect()
            If nErro <> 0
                MsgStop(STR0089 + " " + oMail:GetErrorString(nErro), STR0002) //Falha ao desconectar do servidor
            Endif
        Endif
    Endif

    If nErro == 0
        lRet := .T.
    Endif

Return lRet