#Include 'Protheus.ch'
#Include 'Totvs.ch'

User Function Sugere()

    Local oDlg
    Local lOk := .F.
    Local lCheckFonte := .F.
    Local lCheckBco := .F.
    Local lCheckErro := .F.
    Local lCheckOutro := .F.
    Local oCheck
    Local bFonte := {||ValidaFonte()}
    
    Private cSugestao := ""

    If LOG->LOG_LINHA <= 0
        MsgStop("N�mero de linha inv�lido, informe um n�mero v�lido e tente novamente", "Linha inv�lida")
        Return .F.
    Endif

    DEFINE DIALOG oDlg TITLE "Onde procurar ?" FROM 180, 180 TO 320, 400 PIXEL
        @ 10, 10 CHECKBOX oCheck VAR lCheckFonte PROMPT "Fontes" WHEN (Eval(bFonte)) SIZE 60,15 OF oDlg PIXEL
        @ 20, 10 CHECKBOX oCheck VAR lCheckBco PROMPT "Banco de erros" SIZE 60, 15 OF oDlg PIXEL
        @ 30, 10 CHECKBOX oCheck VAR lCheckErro PROMPT "Pr�prio Erro" SIZE 60, 15 OF oDlg PIXEL
        @ 40, 10 CHECKBOX oCheck VAR lCheckOutro PROMPT "Outros" SIZE 60, 15 OF oDlg PIXEL
        @ 55, 20 BUTTON "Validar" SIZE 35, 10 PIXEL ACTION (lOk := .T., oDlg:End()) WHEN (lCheckFonte .OR. lCheckBco .OR. lCheckErro .OR. lCheckOutro) OF oDlg
    ACTIVATE DIALOG oDlg CENTERED

    If lOk
        Do Case
            Case lCheckFonte
                Processa({||SugereFonte()}, "Aguarde...", "Estamos vasculhando o " + LOG->LOG_FONTE + " para achar uma sugest�o...")
                If !Empty(cSugestao)
                    EECVIEW(cSugestao, "Sugest�es")
                Else
                    EECVIEW("Infelizmente n�o h� sugest�o para a corre��o deste erro!" + CRLF + CRLF + "Contate o suporte para que possamos avaliar o erro e quem sabe melhorar nossa ferramenta ;-)", "Bolso vazio")
                Endif
            Case lCheckBco
                
            Case lCheckErro

            Case lCheckOutro
        End Case
    Endif

Return

Static Function ValidaFonte()

    Local oDirs := EasyUserCfg():New("ERROR")
    Local cDirFontes := oDirs:LoadParam("TUCO","","ERROR")

    If Empty(cDirFontes)
        Return .F.
    Endif

Return .T.

Static Function SugereFonte()

    Local oDirs := EasyUserCfg():New("ERROR")
    Local cDirFontes := oDirs:LoadParam("TUCO","","ERROR")
    Local oFile
    Local cFile := cDirFontes + Alltrim(LOG->LOG_FONTE) //LOcaliza��o do arquivo
    Local aLines := {} //Array que conter� todas as linhas
    Local cLine //Linha onde ocorreu error.log
    Local x, y, z, w
    Local cParametro
    Local aParametro := {} //Array pra conter dados da linha quando tem parametro
    Local aVariaveis := {} //Array para conter as variaveis
    Local aVarRecebe := {} //Array para conter o valor que a variavel recebe
    Local aFunctions := {"IF", "ELSEIF", "ELSE IF", "WHILE", "FOR", "CASE"}
    Local aPreBuilt := {"ENDIF", "ENDDO", "END DO", "DO CASE", "ELSE", "END", "NEXT"} //Array com fun��es do advpl para validar se a linha esta correta
    Local cComentario := ""

    ProcRegua(100)

    If File(cFile)
        oFile := FWFileReader():New(cFile)
        /* Lê cada linha do arquivo e coloca em um array */
        If oFile:Open()
            While(oFile:hasLine())
                aAdd(aLines, oFile:GetLine())
            End
            oFile:Close()
        Endif
        IncProc(30)
        /* Valida se a linha cadastrada no erro ultrapassou a qtde de linhas no fonte */
        If LOG->LOG_LINHA > Len(aLines)
            EECVIEW("O n�mero da linha em que o erro ocorreu � maior que a ultima linha no fonte!" + CRLF + CRLF +  "Linha do erro -> " + cValToChar(LOG->LOG_LINHA) + CRLF + "�ltima linha do fonte -> " + cValToChar(Len(aLines)) + CRLF + CRLF + "Verifique se a data do fonte " + Alltrim(LOG->LOG_FONTE) + " presente em sua pasta " + cDirFontes + " est� correto!","Qtde Linhas Incorretas")
            Return .F.
        Endif
        cLine := aLines[LOG->LOG_LINHA]
        If "//" $ cLine
            cComentario := Alltrim(StrTokArr(cLine, "//")[2])
            cLine := Alltrim(StrTokArr(cLine, "//")[1])
        Elseif "/*" $ cLine
            cComentario := Alltrim(StrTokArr(cLine, "/*")[2])
            cLine := Alltrim(StrTokArr(cLine, "/*")[1])
        Elseif "*/" $ cLine
            cComentario := Alltrim(StrTokArr(cLine, "*/")[2])
            cLine := Alltrim(StrTokArr(cLine, "*/")[1])
        Endif

        If Empty(cLine)
            cSugestao := "N�o h� nenhuma sugest�o a ser dada para este erro!" + Iif(!Empty(cComentario), CRLF + "Mas h� um coment�rio que pode te auxiliar -> " + cComentario, "")
            Return .F.
        Endif
        /* Valida se a linha contem uma fun��o Built-in */
        For x := 1 To Len(aPreBuilt)
            If aPreBuilt[x] $ Upper(cLine)
                EECVIEW("A linha onde foi apresentado o erro n�o � v�lida por conter a Built-in Function " + aPreBuilt[x] + ", verifique se a data do fonte est� correta", "Linha incorreta")
                Return .F.
            Endif
        Next x

        /* Valida se a linha contem um parametro */
        If "MV_" $ cLine
            aParametro := StrTokArr(cLine, '"')
            For x := 1 To Len(aParametro)
                If "MV_" $ aParametro[x]
                    cSugestao += "Verifique se o valor do par�metro " + aParametro[x] + " est� correto e realize um teste alterando seu valor" + CRLF
                Endif
            Next x
        Endif

        /* Valida variaveis */
        For x := 1 To Len(aFunctions)
            If aFunctions[x] $ Upper(cLine)
                StrTran(cLine, aFunctions[x])
            Endif
        Next x
        aVariaveis := StrTokArr(cLine, " ")
        IncProc(10)
        For x := 1 To Len(aVariaveis)
            If Alltrim(aVariaveis[x]) $ LOG->LOG_ERRO
                For y := 1 To Len(aLines)
                    If Alltrim(aVariaveis[x]) $ aLines[y] .AND. ":=" $ aLines[y]
                        aVarRecebe := StrTokArr(aLines[y], " ")
                        For z := 1 To Len(aVarRecebe)
                            If ":=" $ aVarRecebe[z]
                                If z > 1 .AND. Alltrim(aVariaveis[x]) $ aVarRecebe[z - 1]
                                    If Len(aVarRecebe) >= z + 1 .AND. "MV_" $ aVarRecebe[z + 1]
                                        aParametro := StrTokArr(aVarRecebe[z + 1], '"')
                                        For w := 1 To Len(aParametro)
                                            If "MV_" $ aParametro[w]
                                                cParametro := aParametro[w]
                                            Endif
                                        Next w
                                        cSugestao += "Verifique se o valor do par�metro " + cParametro + " est� correto e realize um teste alterando seu valor" + CRLF
                                    ElseIf Len(aVarRecebe) >= z + 1 .AND. "(" $ aVarRecebe[z + 1] .AND. ")" $ aVarRecebe[z + 1]
                                        cSugestao += "A variavel que apresenta erro est� recebendo valor da fun��o " + aVarRecebe[z + 1] + ", Verifique a fun��o" + CRLF
                                    Endif
                                Endif
                            Endif
                        Next z
                    Endif
                Next y
            Endif
        Next x
    Else
        MsgStop("Fonte n�o localizado no diret�rio informado", "Aten��o")
    Endif

Return