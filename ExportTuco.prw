#Include 'Protheus.ch'
#Include 'Totvs.ch'
#Include 'Tuco.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} U_ExportData
Exporta dados para Excel e Txt

@type       User Function

@author     Jo�o Pedro
@since      16/01/2018
@version    P12
/*/
//-------------------------------------------------------------------
User Function ExportData()

    Local oExporta := ExportaDados():New()                  //Instancia classe de exporta��o de dados

    Processa({||oExporta:MontaWork()},STR0006,STR0033,.F.)  //Chama fun��o para montar os tempor�rios

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} ExportaDados
Classe principal da exporta��o de dados

@type       Class

@author     Jo�o Pedro
@since      16/01/2018
@version    P12
/*/
//-------------------------------------------------------------------
Class ExportaDados From Tuco

    Method New() Constructor
    Method MontaWork()  //Monta tempor�rio para exportar dados
    Method MontaTela()  //Monta tela para informar dados do arquivo
    Method ChooseFile() //Abre tela para selecionar arquivo
    Method geraExcel()  //Gera arquivo em Excel

End Class

//-------------------------------------------------------------------
/*/{Protheus.doc} New
Instancia variaveis necess�rias

@type       Class

@author     Jo�o Pedro
@since      16/01/2018
@version    P12
/*/
//-------------------------------------------------------------------
Method New() Class ExportaDados
Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} MontaWork
Monta arquivo tempor�rio para exportar dados

@type       Method

@author     Jo�o Pedro
@since      16/01/2018
@version    P12

@return     Self
/*/
//-------------------------------------------------------------------
Method MontaWork() Class ExportaDados

    Local x
    Local aEstrutura := {}                                                                                                  //Array que guarda a estrutura do tempor�rio

    SX3->(DbSetOrder(2))                                                                                                    //Seta a ordem na SX3

    For x := 1 To Len(LOG->(DbStruct()))
        AADD(aEstrutura, {LOG->(DbStruct())[x][1],LOG->(DbStruct())[x][2],LOG->(DbStruct())[x][3],LOG->(DbStruct())[x][4]}) //Adiciona na estrutura do tempor�rio os campos da tabela LOG
    Next x

    cFileTxt := E_CriaTrab(,aEstrutura,"WorkTxt")                                                                           //Cria �rea de trabalho

    /* Caso n�o consiga criar */
    If !USED()
        MsgStop(STR0034,STR0002)                                                                                            //Eu falei que dava trabalho esses arquivos tempor�rios, deu algum problema para criar eles
        Return .F.
    Endif

    IndRegua("WorkTxt",cFileTxt+TEOrdBagExt(),"LOG_ERRO+LOG_FUNC+LOG_FONTE")                                                //Cria indices

    LOG->(DbGoTop())                                                                                                        //Posiciona no primeiro registro da tabela LOG
    WorkTxt->(DbGoTop())                                                                                                    //Posiciona no primeiro registro da Work
    ProcRegua(LOG->(LastRec()))

    While LOG->(!Eof())

        IncProc("Processando erro: " + LOG->LOG_ERRO)

        /*Popula arquivo tempor�rio */
        WorkTxt->(DbAppend())
        WorkTxt->LOG_FILIAL     := LOG->LOG_FILIAL
        WorkTxt->LOG_ERRO       := LOG->LOG_ERRO
        WorkTxt->LOG_FUNC       := LOG->LOG_FUNC
        WorkTxt->LOG_FONTE      := LOG->LOG_FONTE
        WorkTxt->LOG_LINHA      := LOG->LOG_LINHA
        WorkTxt->LOG_DT_FT      := LOG->LOG_DT_FT
        WorkTxt->LOG_TP_BC      := LOG->LOG_TP_BC
        WorkTxt->LOG_LINGUA     := LOG->LOG_LINGUA
        WorkTxt->LOG_EXT        := LOG->LOG_EXT
        WorkTxt->LOG_SYSTEM     := LOG->LOG_SYSTEM
        WorkTxt->LOG_LIB        := LOG->LOG_LIB
        WorkTxt->LOG_SERVER     := LOG->LOG_SERVER
        WorkTxt->LOG_BANC       := LOG->LOG_BANC
        WorkTxt->LOG_DBACES     := LOG->LOG_DBACES
        WorkTxt->LOG_REL        := LOG->LOG_REL
        WorkTxt->LOG_LIC        := LOG->LOG_LIC
        WorkTxt->LOG_SOL        := LOG->LOG_SOL
        WorkTxt->LOG_PILHA      := LOG->LOG_PILHA
        WorkTxt->LOG_USER       := LOG->LOG_USER
        WorkTxt->LOG_SIMB1      := LOG->LOG_SIMB1
        WorkTxt->LOG_SIMB2      := LOG->LOG_SIMB2
        WorkTxt->LOG_SIMB3      := LOG->LOG_SIMB3
        WorkTxt->LOG_SIMB4      := LOG->LOG_SIMB4
        WorkTxt->LOG_SIMB5      := LOG->LOG_SIMB5
        WorkTxt->LOG_IP         := LOG->LOG_IP
        WorkTxt->LOG_PORT       := LOG->LOG_PORT
        WorkTxt->LOG_EMP        := LOG->LOG_EMP

        LOG->(DbSkip())

    Enddo

    WorkTxt->(DbGoTop())
    Processa({||::MontaTela()},STR0006,STR0035,.F.)                                                                     //Tela para informar dados do arquivo

    WorkTxt->(E_EraseArq(cFileTxt))                                                                                     //Exclui arquivo tempor�rio gerado

    LOG->(DbGoTop())

Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} MontaTela
Monta tela para informar nome do arquivo

@type       Method

@author     Jo�o Pedro
@since      16/01/2018
@version    P12

@return     Self
/*/
//-------------------------------------------------------------------
Method MontaTela() Class ExportaDados

    Local oDlg
    Local cApp      := ""                                                                                   //Variavel que guardara o tipo do arquivo (TXT ou Excel)
    Local cFile     := Space(07)                                                                            //Variavel que guardara o nome do arquivo
    Local cDir      := Space(30)                                                                            //Variavel que guardara o diretorio onde sera gravado o arquivo
    Local aApp      := {"TXT", "Excel"}                                                                     //Vetor para o combobox
    Local nOpc      := 0                                                                                    //Variavel que guardara a op��o escolhida pelo usuario (OK, Cancelar)
    Local cDirStart :=Upper(GetSrvProfString("STARTPATH",""))                                               //Variavel que guardara caminho onde ser� salvo o arquivo
    Local bArqValid := {||IF(TR350VALID(2,cFile).AND.TR350VALID(3,cDir),MsgYesNo(STR0036,STR0002),.F.)}     //Valida��o do arquivo

    Private lValExcel := .F.

    IF(Right(cDirStart,1) != "\", cDirStart += "\",)

    /* Monta tela */
    DEFINE MSDIALOG oDlg TITLE STR0037 From 9,0 To 20,50 OF oMainWnd                                        //Exporta Dados
    oPanel := TPanel():New(0,0,"",oDlg,,.F.,.F.,,,90,165)
    oPanel:Align := CONTROL_ALIGN_ALLCLIENT

    @ 10,03 SAY STR0038 OF oPanel PIXEL                                                                     //Tipo do arquivo
    @ 23,03 SAY STR0039 OF oPanel PIXEL                                                                     //Nome Arquivo
    @ 36,03 SAY STR0040 OF oPanel PIXEL                                                                     //Diret�rio
    @ 10,45 COMBOBOX cApp ITEMS aApp SIZE 35,10 OF oPanel PIXEL
    @ 23,45 MSGET cFile SIZE 30,7 OF oPanel PIXEL
    @ 36,45 MSGET cDir  SIZE 95,8 PICTURE '@!' OF oPanel PIXEL WHEN .F.
    @ 35,150 BUTTON STR0041 SIZE 38,12 ACTION If(!Empty(cNewDir := ::ChooseFile()), cDir := cNewDir, ) OF oPanel Pixel //Alterar

     ACTIVATE MSDIALOG oDlg ON INIT ;
            EnchoiceBar(oDlg,{||nOpc:=1,IF(EVAL(bArqValid),oDlg:End(),nOpc:=0)},;
                             {||oDlg:End()}) CENTERED

    If nOpc == 0
        Return .F.
    Endif

    If Empty(cFile)
        cFile:= CriaTrab(, .F.)
    EndIf

    xFile := cFile

    lConfirma := .T.

    cFileAux := Alltrim(cDirStart)+AllTrim(cFile)

    If cApp == "TXT" .AND. Left(cDir,1) == "\"
        cDir := GetTempPath()
    Endif

    cFile := Alltrim(cDir)+If(Right(Alltrim(cDir),1)="\","","\")+AllTrim(cFile)

    IF FILE(cFile+"."+cApp)
        lConfirma:=MsgYesNo(STR0042,STR0002) //Eu posso matar o arquivo que j� tem na pasta e criar este novo ?
    Endif

    If lConfirma

        IF FILE(cFileAux+"."+cApp)
            ERASE(cFileAux+"."+cApp)
        ENDIF

        ERASE(cFile+"."+cApp)

        If cApp == "TXT"
            COPY TO (cFileAux+"."+cApp) SDF
            If cFileAux <> cFile
                If !AvCpyFile(cFileAux+"."+cApp,cFile+"."+cApp,.T.)
                    MsgAlert(STR0043 + cFile+"."+cApp,STR0002) //Problema ao copiar arquivo
                Else
                    ERASE(cFileAux+"."+cApp)
                Endif
            Endif
            ShellExecute("open", Alltrim(cFile)+"."+cApp,"","",1)
        Elseif cApp == "Excel"
            ::geraExcel(cDir, StrTokArr(cFile, "\")[Len(StrTokArr(cFile, "\"))])
        Endif
    Endif

Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} ChooseFile
Abre tela para sele��o de arquivo

@type       Method

@author     Jo�o Pedro
@since      16/01/2018
@version    P12

@return     cFile {String}, Nome do arquivo
/*/
//-------------------------------------------------------------------
Method ChooseFile() Class ExportaDados

    Local cTitle            := STR0045 //Selecione o diret�rio para grava��o do arquivo.
    Local nDefaultMask      := 0
    Local cDefaultDir       := "C:\"
    Local nOptions          := GETF_OVERWRITEPROMPT+GETF_LOCALHARD+GETF_NETWORKDRIVE+GETF_RETDIRECTORY
    Local cFile             := cGetFile(,cTitle,nDefaultMask,cDefaultDir,,nOptions)

Return cFile

//-------------------------------------------------------------------
/*/{Protheus.doc} geraExcel
Exportar arquivo para Excel

@type       Method

@author     Jo�o Pedro
@since      16/01/2018
@version    P12

@return     Self
/*/
//-------------------------------------------------------------------
Method geraExcel(cDir, cFileName) Class ExportaDados

    Local oExcel := FWMsExcel():New()

    oExcel:AddWorkSheet("Com Solucao")
    oExcel:AddTable("Com Solucao", "Erros")
    oExcel:AddColumn("Com Solucao", "Erros", "Erro", 2, 1, .F.)
    oExcel:AddColumn("Com Solucao", "Erros", "Fun��o", 2, 1, .F.)
    oExcel:AddColumn("Com Solucao", "Erros", "Fonte", 2, 1, .F.)
    oExcel:AddColumn("Com Solucao", "Erros", "Linha", 2, 2, .F.)
    oExcel:AddColumn("Com Solucao", "Erros", "Data Fonte", 2, 4, .F.)
    oExcel:AddColumn("Com Solucao", "Erros", "Local Files", 2, 1, .F.)
    oExcel:AddColumn("Com Solucao", "Erros", "Banco", 2, 1, .F.)
    oExcel:AddColumn("Com Solucao", "Erros", "Release", 2, 1, .F.)
    oExcel:AddColumn("Com Solucao", "Erros", "Solu��o", 2, 1, .F.)
    oExcel:AddWorkSheet("Sem Solucao")
    oExcel:AddTable("Sem Solucao", "Erros")
    oExcel:AddColumn("Sem Solucao", "Erros", "Erro", 2, 1, .F.)
    oExcel:AddColumn("Sem Solucao", "Erros", "Fun��o", 2, 1, .F.)
    oExcel:AddColumn("Sem Solucao", "Erros", "Fonte", 2, 1, .F.)
    oExcel:AddColumn("Sem Solucao", "Erros", "Linha", 2, 2, .F.)
    oExcel:AddColumn("Sem Solucao", "Erros", "Data Fonte", 2, 4, .F.)
    oExcel:AddColumn("Sem Solucao", "Erros", "Local Files", 2, 1, .F.)
    oExcel:AddColumn("Sem Solucao", "Erros", "Banco", 2, 1, .F.)
    oExcel:AddColumn("Sem Solucao", "Erros", "Release", 2, 1, .F.)
    oExcel:AddColumn("Sem Solucao", "Erros", "Solu��o", 2, 1, .F.)

    LOG->(DbGoTop())
    Do While LOG->(!Eof())
        If !Empty(LOG->LOG_SOL)
            oExcel:AddRow("Com Solucao", "Erros", {LOG->LOG_ERRO, LOG->LOG_FUNC, LOG->LOG_FONTE, LOG->LOG_LINHA, LOG->LOG_DT_FT, LOG->LOG_EXT, LOG->LOG_BANC, LOG->LOG_REL, LOG->LOG_SOL})
        Else
            oExcel:AddRow("Sem Solucao", "Erros", {LOG->LOG_ERRO, LOG->LOG_FUNC, LOG->LOG_FONTE, LOG->LOG_LINHA, LOG->LOG_DT_FT, LOG->LOG_EXT, LOG->LOG_BANC, LOG->LOG_REL, LOG->LOG_SOL})
        Endif
        LOG->(DbSkip())
    End

    oExcel:Activate()
    oExcel:GetXMLFile(cFileName + ".xml")

    If CpyS2T(cFileName + ".xml", cDir)
        MsgInfo(STR0074 + " " + cDir, STR0032) //Eu coloquei um arquivo .XML no diret�rio, ele � compat�vel com o Excel...Da uma olhada l�
    Else
        MsgStop(STR0075 + " " + cDir, STR0032) //Os guardas do servidor me pegaram roubando o arquivo e n�o consegui gravar no diret�rio
    Endif

Return Self