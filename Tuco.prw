#Include 'Protheus.ch'
#Include 'Totvs.ch'
#Include 'Tuco.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} U_Tuco
Fun��o principal do Tuco

@type       User Function

@author     Jo�o Pedro
@since      08/01/2018
@version    P12
/*/
//-------------------------------------------------------------------
User Function Tuco()

    Private oTuco := Tuco():New() //Instancia a classe principal

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} Tuco
Classe principal do Tuco

@type       Class

@author     Jo�o Pedro
@since      05/12/2019
@version    P12
/*/
//-------------------------------------------------------------------
Class Tuco

    Data aRet           as Array    //Necess�rio para n�o gerar error.log ao entrar na rotina

    Method New() Constructor
    Method ValidaTable()            //M�todo de valida��o da tabela LOG
    //Method MontaDirs()  
    Method TableAttDef()            //M�todo de montagem das Views do Browse
    Method GraphTuco(oBrowse)       //M�todo de montagem dos gr�ficos do Browse
    Method TranfCon()
    Method ValidDate()

End Class

//-------------------------------------------------------------------
/*/{Protheus.doc} Tuco
M�todo construtor da Classe Tuco

@type       Method

@author     Jo�o Pedro
@since      05/12/2019
@version    P12

@return     Self
/*/
//-------------------------------------------------------------------
Method New() Class Tuco

    Local oPin      := DeOpiniao():New() //Instancia a classe para o Wizard de sugest�o de melhorias
    Local aCores    := {}                //L�gica das legendas do browse

    Private aRotina   := {}                //Menus do browse princiapl
    Private cCadastro := STR0003         //Tuco
    //Private cFiltro   := "SELECT * FROM LOG990"

    aCores  := {{'!Empty(LOG->LOG_SOL) .OR. !Empty(LOG->LOG_REFARQ)','ENABLE'},; //Verde     -> Solu��o ou arquivo de refer�ncia preenchidos
            {'Empty(LOG->LOG_SOL) .AND. Empty(LOG->LOG_REFARQ) .AND. Empty(LOG->LOG_SOLCON) .AND. Empty(LOG->LOG_REFCON)','DISABLE'},;                                  //Vermelho  -> Solu��o vazia
            {'!Empty(LOG->LOG_SOLCON) .OR. !Empty(LOG->LOG_REFCON)', 'BR_LARANJA'}}

    If !ExistDir("\Tuco_Erros\")
        If MakeDir("\Tuco_Erros") != 0
            MsgStop(STR0114, STR0002) //"N�o foi poss�vel criar o diret�rio para armazenamento dos arquivos de erro"
        Endif
    Endif                                

    If ::ValidaTable() //Valida se a tabela Log existe no ambiente
    
        ::ValidDate()
        //SetKey(VK_F11, {||::MontaDirs()})
        SetKey(VK_F12, {||oPin:MontaWizard()})  //Seta o wizard de sugest�o de melhoria na tecla F12 do browse

        aRotina := MenuDef()                    //Grava as op��es do menu no aRotina
        LOG->(DbSetOrder(3))                    //Seta a ordem do indice da tabela para que o Browse seja ordenado por fonte

        /* Ponto de entrada a ser utilizado para adicionar op��es ao browse atrav�s da variavel aRotina */
        If ExistBlock("TUCOPE")
            Execblock("TUCOPE",.F.,.F., "ORDEM_BROWSE")
        Endif
        
        mBrowse(6,1,22,75,"LOG",,,,,,aCores,,,,,,,,/*cFiltro*/,,,,, {|oBrowse|::GraphTuco(oBrowse)} ) //Abre o browse principal do Tuco

    Endif

Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} ValidaTable
M�todo para validar se a tabela LOG existe no ambiente

@type       Method

@author     Jo�o Pedro
@since      12/12/2018
@version    P12

@return     Logical, Tabela existe ou n�o {.T., .F.}
/*/
//-------------------------------------------------------------------
Method ValidaTable() Class Tuco

    If !ChkFile("LOG") //Valida se a tabela LOG est� criada no ambiente
        MsgStop(STR0001,STR0002) //Vou dizer mais uma vez, crie a tabela para utilizar a rotina!
        Return .F.
    Endif

Return .T.

/*
Method MontaDirs() Class Tuco

    Local oDirs := EasyUserCfg():New("ERROR")
    Local oDlg
    Local lRet := .F.
    Local nLin := 25
    Local nCol := 12
    Local cDirFontes := oDirs:LoadParam("TUCO","","ERROR") //Carrega o diretorio anteriormente informado
    Local bDir := {|| cDirFontes := cGetFile("",STR0005, 0, cDirFontes,,GETF_OVERWRITEPROMPT+GETF_LOCALHARD+GETF_NETWORKDRIVE+GETF_RETDIRECTORY) }
    Local bOk := {|| lRet := .T., oDlg:End() }
    Local bCancel := {|| oDlg:End() }

    DEFINE MSDIALOG oDlg TITLE STR0004 FROM 320, 400 TO 580, 785 OF oMainWnd PIXEL

        oPanel:= TPanel():New(0, 0, "", oDlg,, .F., .F.,,, 90, 165)
        oPanel:Align:= CONTROL_ALIGN_ALLCLIENT

        @ nLin, nCol Say STR0005 Size 160, 08 PIXEL OF oPanel
        nLin += 10
        @ nLin, nCol MsGet cDirFontes Size 150, 08 PIXEL OF oPanel
        @ nLin, nCol+150 BUTTON "..." ACTION Eval(bDir) Size 10, 10 PIXEL OF oPanel

    ACTIVATE MSDIALOG oDlg ON INIT EnchoiceBar(oDlg, bOk, bCancel) CENTERED

    If lRet
        oDirs:SetParam("TUCO", cDirFontes, "ERROR") //Seta o novo diret�rio
    Endif

Return Self
*/

//-------------------------------------------------------------------
/*/{Protheus.doc} TableAttDef
M�todo que contr�i as Views e gr�ficos do Browse principal

@type       Method

@author     Jo�o Pedro
@since      08/01/2018
@version    P12

@return     Object, Objeto da classe FWTableAtt contendo as views {oTableAtt}
/*/
//-------------------------------------------------------------------
Method TableAttDef() Class Tuco

    /* Objetos das views */
    Local oFrame    := Nil
    Local oEIC      := Nil
    Local oEEC      := Nil
    Local oEFF      := Nil
    Local oEDC      := Nil
    Local oESS      := Nil
    Local oFIN      := Nil
    Local oCOM      := Nil
    Local oGen      := Nil
    Local oCustom   := Nil
    Local oPorFrame := Nil
    Local oTableAtt := FWTableAtt():New() //Instancia a classe principal para cria��o das views

    oTableAtt:SetAlias("LOG") //Seta a tabela respons�vel pelos dados

    oFrame := FWDSView():New()
    oFrame:SetName(STR0094)                                                                             //Framework
    oFrame:SetID(STR0094)                                                                              //Identifica��o da view
    oFrame:SetOrder(1)
    oFrame:SetCollumns({"LOG_ERRO","LOG_FUNC","LOG_FONTE","LOG_SOL"})                                //Colunas da view
    oFrame:SetPublic(.T.)                                                                           //View Publica
    oFrame:AddFilter(STR0094,"'LIB' $ LOG_FONTE .OR. 'BROWSE' $ LOG_FONTE .OR. 'CFG' $ LOG_FONTE .OR. 'MS' $ Left(LOG_FONTE,2) .OR. 'FW' $ Left(LOG_FONTE,2)") //Filtro da view
    oTableAtt:AddView(oFrame)                                                                     //Adiciona view ao browse

    oEIC := FWDSView():New()
    oEIC:SetName(STR0095) //Importa��o
    oEIC:SetID(STR0095)
    oEIC:SetOrder(1)
    oEIC:SetCollumns({"LOG_ERRO","LOG_FUNC","LOG_FONTE","LOG_SOL"})
    oEIC:SetPublic(.T.)
    oEIC:AddFilter(STR0095,"'EIC' $ LOG_FONTE")
    oTableAtt:AddView(oEIC)

    oEEC := FWDSView():New()
    oEEC:SetName(STR0096) //Exporta��o
    oEEC:SetID(STR0096)
    oEEC:SetOrder(1)
    oEEC:SetCollumns({"LOG_ERRO","LOG_FUNC","LOG_FONTE","LOG_SOL"})
    oEEC:SetPublic(.T.)
    oEEC:AddFilter(STR0096,"'EEC' $ LOG_FONTE")
    oTableAtt:AddView(oEEC)

    oEFF := FWDSView():New()
    oEFF:SetName(STR0097) //Financing
    oEFF:SetID(STR0097)
    oEFF:SetOrder(1)
    oEFF:SetCollumns({"LOG_ERRO","LOG_FUNC","LOG_FONTE","LOG_SOL"})
    oEFF:SetPublic(.T.)
    oEFF:AddFilter(STR0097,"'EFF' $ LOG_FONTE")
    oTableAtt:AddView(oEFF)

    oEDC := FWDSView():New()
    oEDC:SetName(STR0098) //Drawback
    oEDC:SetID(STR0098)
    oEDC:SetOrder(1)
    oEDC:SetCollumns({"LOG_ERRO","LOG_FUNC","LOG_FONTE","LOG_SOL"})
    oEDC:SetPublic(.T.)
    oEDC:AddFilter(STR0098,"'EDC' $ LOG_FONTE")
    oTableAtt:AddView(oEDC)

    oESS := FWDSView():New()
    oESS:SetName(STR0099) //Siscoserv
    oESS:SetID(STR0099)
    oESS:SetOrder(1)
    oESS:SetCollumns({"LOG_ERRO","LOG_FUNC","LOG_FONTE","LOG_SOL"})
    oESS:SetPublic(.T.)
    oESS:AddFilter(STR0099,"'ESS' $ LOG_FONTE")
    oTableAtt:AddView(oESS)

    oFIN := FWDSView():New()
    oFIN:SetName(STR0100) //Financeiro
    oFIN:SetID(STR0100)
    oFIN:SetOrder(1)
    oFIN:SetCollumns({"LOG_ERRO","LOG_FUNC","LOG_FONTE","LOG_SOL"})
    oFIN:SetPublic(.T.)
    oFIN:AddFilter(STR0100,"'FINA' $ LOG_FONTE .AND. !'FINAL' $ LOG_FONTE")
    oTableAtt:AddView(oFIN)

    oCOM := FWDSView():New()
    oCOM:SetName(STR0101) //Compras
    oCOM:SetID(STR0101)
    oCOM:SetOrder(1)
    oCOM:SetCollumns({"LOG_ERRO","LOG_FUNC","LOG_FONTE","LOG_SOL"})
    oCOM:SetPublic(.T.)
    oCOM:AddFilter(STR0101,"'MATA' $ LOG_FONTE")
    oTableAtt:AddView(oCOM)
    
    oGen := FWDSView():New()
    oGen:SetName(STR0102) //Gen�ricos
    oGen:SetID(STR0102)
    oGen:SetOrder(1)
    oGen:SetCollumns({"LOG_ERRO","LOG_FUNC","LOG_FONTE","LOG_SOL"})
    oGen:SetPublic(.T.)
    oGen:AddFilter(STR0102,"!'MATA' $ LOG_FONTE .AND. !'FINA' $ LOG_FONTE .AND. !'ESS' $ LOG_FONTE .AND. !'EDC' $ LOG_FONTE .AND. !'EFF' $ LOG_FONTE .AND. !'EEC' $ LOG_FONTE .AND. !'EIC' $ LOG_FONTE .AND. !'LIB' $ LOG_FONTE .AND. !'FINAL' $ LOG_FONTE .AND. !'MS' $ Left(LOG_FONTE, 2) .AND. !'FW' $ Left(LOG_FONTE, 2)")
    oTableAtt:AddView(oGen)

    oCustom := FWDSView():New()
    oCustom:SetName(STR0118) //Customiza��es
    oCustom:SetID(STR0118)
    oCustom:SetOrder(1)
    oCustom:SetCollumns({"LOG_ERRO", "LOG_FUNC", "LOG_FONTE", "LOG_SOL"})
    oCustom:SetPublic(.T.)
    oCustom:AddFilter(STR0118, "'U_' $ LOG_FUNC")
    oTableAtt:AddView(oCustom)

    /* Gr�ficos */
    oPorFrame := FWDSChart():New()
    oPorFrame:SetName(STR0103)  //Fontes
    oPorFrame:SetTitle(STR0103) //Titulo do gr�fico
    oPorFrame:SetID("PorFrame") //Identifica��o do gr�fico
    oPorFrame:SetType("BARCOMPCHART") //Tipo do gr�fico
    oPorFrame:SetSeries({{"LOG","LOG_FONTE","COUNT"}})
    oPorFrame:SetCategory({{"LOG","LOG_FONTE"}})
    oPorFrame:SetPublic( .T. )  //Gr�fico p�blico
    oPorFrame:SetLegend( CONTROL_ALIGN_BOTTOM ) //Inferior
    oPorFrame:SetTitleAlign( CONTROL_ALIGN_CENTER ) 
    oTableAtt:AddChart(oPorFrame) //Adiciona gr�fico ao browse

    /* Gr�ficos */
    oPorFrame := FWDSChart():New()
    oPorFrame:SetName(STR0116)  //Fun��es
    oPorFrame:SetTitle(STR0116) //Titulo do gr�fico
    oPorFrame:SetID("Func") //Identifica��o do gr�fico
    oPorFrame:SetType("BARCOMPCHART") //Tipo do gr�fico
    oPorFrame:SetSeries({{"LOG","LOG_FUNC","COUNT"}})
    oPorFrame:SetCategory({{"LOG","LOG_FUNC"}})
    oPorFrame:SetPublic( .T. )  //Gr�fico p�blico
    oPorFrame:SetLegend( CONTROL_ALIGN_BOTTOM ) //Inferior
    oPorFrame:SetTitleAlign( CONTROL_ALIGN_CENTER ) 
    oTableAtt:AddChart(oPorFrame) //Adiciona gr�fico ao browse

Return oTableAtt

//-------------------------------------------------------------------
/*/{Protheus.doc} GraphTuco
M�todo que seta no browse o gr�fico e views

@type       Method

@author     Jo�o Pedro
@since      29/11/2019
@version    P12

@param      oBrowse
@param      [oBrowse], Object, Browse que o gr�fico ser� aplicado

@return     Self
/*/
//-------------------------------------------------------------------
Method GraphTuco(oBrowse) Class Tuco

    Local oTableAtt := ::TableAttDef() //Instancia a classe principal da montagem do gr�fico e Views

    oBrowse:SetAttach(.T.)                      //Adiciona a op��o do gr�fico ao lado direito do browse
    oBrowse:SetViewsDefault(oTableAtt:aViews)   //Seta as views criadas na classe TableAttDef no browse
    oBrowse:SetChartsDefault(oTableAtt:aCharts) //Seta os gr�ficos criados na classe TableAttDef no browse

Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} MenuDef
Menus do Browse principal do Tuco

@type       Static Function

@author     Jo�o Pedro
@since      08/01/2019
@version    P12

@return     Array, Menus a serem apresentados no Browse {aRotina}
/*/
//-------------------------------------------------------------------
Static Function MenuDef()

    Local aRotina   := {}
    Local aRotAuto  := {{STR0058,"U_splitError",0,3},;   //Auto Erro
                        {STR0059,"U_AutoDue",0,7}}      //Auto DU-E

    AADD(aRotina, {STR0062, 'AxPesqui',0,1})            //Pesquisar
    AADD(aRotina, {STR0063,"VIEWDEF.TUCO",0,2})         //Visualizar
    AADD(aRotina, {STR0064,"VIEWDEF.TUCO",0,3})         //Manual
    AADD(aRotina, {STR0065,"VIEWDEF.TUCO",0,4})         //Alterar
    AADD(aRotina, {STR0066,"VIEWDEF.TUCO",0,5})         //Excluir
    AADD(aRotina, {STR0067,"U_TucoLeg",0,6})              //Legenda
    AADD(aRotina, {STR0060,"U_ExportData",0,9})         //Exporta dados
    AADD(aRotina, {STR0068,aRotAuto,0,0})               //Automatico

Return aRotina

//-------------------------------------------------------------------
/*/{Protheus.doc} TucoLeg
Define as legendas do browse

@type       Function

@author     Jo�o Pedro
@since      08/01/2018
@version    P12

@return     Array, Op��es de legenda do Browse {aLegenda}
/*/
//-------------------------------------------------------------------
User Function TucoLeg()

    Local aLegenda := {}

    AADD(aLegenda, {"BR_VERDE", STR0070})       //Tem solu��o
    AADD(aLegenda, {"BR_VERMELHO", STR0071})    //Sem solu��o
    AADD(aLegenda, {"BR_LARANJA", STR0109})     //Conting�ncia

    BrwLegenda(cCadastro, STR0067, aLegenda)    //Legenda

Return aLegenda

//-------------------------------------------------------------------
/*/{Protheus.doc} ModelDef
Modelo de dados do Tuco (Padr�o MVC)

@type       Static Function

@author     Jo�o Pedro
@since      29/11/2019
@version    P12

@return     Object, Modelo de dados {oModel}
/*/
//-------------------------------------------------------------------
Static Function ModelDef()

    Local oModel
    Local oStruLog := FWFormStruct(1, "LOG")                            //Instancia a estrutura do modelo de dados conforme tabela LOG
    Local oStruZZZ := FWFormStruct(1, "ZZZ")

    oModel := MPFormModel():New('MODLOG')                               //Instancia o modelo de dados
    oModel:AddFields('LOGF',,oStruLog)                                  //Adiciona os campos da tabela LOG no modelo
    oModel:AddGrid("ZZZD","LOGF", oStruZZZ)
    oModel:GetModel("LOGF"):SetDescription(STR0003)                     //Tuco
    oModel:GetModel("ZZZD"):SetDescription(STR0117)                     //Contatos
    oModel:SetRelation("ZZZD", {{"ZZZ_FILIAL", "FwXFilial('ZZZ')"}, {"ZZZ_ERRO", "LOG_ERRO"}}, ZZZ->(IndexKey(1)))
    oModel:SetPrimaryKey({'LOG_FILIAL', 'LOG_ERRO', 'LOG_FUNC'})        //Chave prim�ria a ser utilizada no modelo de dados
    //oModel:GetModel("ZZZD"):SetPrimaryKey({'ZZZ_FILIAL', 'ZZZ_NOME', 'ZZZ_TELEFONE'})

Return oModel

//-------------------------------------------------------------------
/*/{Protheus.doc} ViewDef
Visualiza��o de dados do Tuco (Padr�o MVC)

@type       Static Function

@author     Jo�o Pedro
@since      29/11/2019
@version    P12

@return     Object, View de dados {oView}
/*/
//-------------------------------------------------------------------
Static Function ViewDef()

    Local oModel    := FWLoadModel("Tuco")                              //Carrega o modelo de dados definido pela fun��o ModelDef
    Local oStruLog  := FWFormStruct(2,"LOG")                            //Carrega a estrutura de dads da tabela LOG
    Local oStruZZZ  := FWFormStruct(2, "ZZZ")
    Local oView
    Local oAnexo    := Anexo():New()                                    //Instancia a classe Anexo para futura utiliza��o de bot�es
    Local bCont     := {||Iif(Empty(LOG->LOG_SOL) .AND. Empty(LOG->LOG_REFARQ) .AND. (!Empty(LOG->LOG_SOLCON) .OR. !Empty(LOG->LOG_REFCON)), Iif(MsgYesNo(STR0108), TranfCon(), ""), "")}
    Local bExclude  := {|oView|Iif(oView:GetOperation() == 5, ExcluiErro(), "")} 

    oView := FWFormView():New()                                         //Instancia a ViewDef
    oView:Refresh()
    oView:SetModel(oModel)                                              //Seta o modelo a ser utilizado pela ViewDef
    oView:AddField('VIEW_TUCO',oStruLog, 'LOGF')                                     //Adiciona os campos a View principal
    oView:AddGrid('VIEW_CONT', oStruZZZ, 'ZZZD')
    oView:CreateHorizontalBox("SUPERIOR", 70)
    oView:CreateHorizontalBox("INFERIOR", 30)
    oView:SetOwnerView('VIEW_TUCO', "SUPERIOR")
    oView:SetOwnerView('VIEW_CONT', "INFERIOR")
    oView:SetAfterViewActivate(bCont)
    oView:SetAfterOkButton(bExclude)
    //oView:addUserButton(STR0069,"NOTE",{||U_Sugere()})
    oView:addUserButton(STR0061,"NOTE",{||oAnexo:paramAnexo()})         //Anexos
    oView:addUserButton(STR0091,"NOTE",{||oAnexo:limpaAnexo()})         //Limpar Anexo
    oView:SetProgressBar(.T.)                                           //Define que ir� utilizar barra de progresso na View
    oView:EnableControlBar(.T.)

Return oView

//-------------------------------------------------------------------
/*/{Protheus.doc} TranfCon
Realiza a efetiva��o da solu��o de conting�ncia

@type       Static Function

@author     Jo�o Pedro
@since      14/02/2020
@version    P12

@return     Object, View de dados {oView}
/*/
//-------------------------------------------------------------------
Static Function TranfCon()

    Local oAnexo := Anexo():New()

    If !Empty(LOG->LOG_SOL) .OR. !Empty(LOG->LOG_REFARQ)
        If MsgYesNo(STR0110, STR0002) // "Existe uma solu��o definida para este erro. Podemos apagar a conting�ncia ?"
            RecLock("LOG", .F.)
            LOG->LOG_SOLCON := ""
            oAnexo:limpaAnexo()
            LOG->(MsUnlock())
        Endif
    Endif

    RecLock("LOG", .F.)
    If !Empty(LOG->LOG_SOLCON)
        LOG->LOG_SOL := LOG->LOG_SOLCON
        LOG->LOG_SOLCON := ""
    Endif
    If !Empty(LOG->LOG_REFCON)
        LOG->LOG_REFARQ := LOG->LOG_REFCON
        LOG->LOG_REFCON := ""
    Endif
    LOG->(MsUnlock())

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} ExcluiErro
Exclui o arquivo de error.log do servidor quando o erro � excluido da base

@type       Static Function

@author     Jo�o Pedro
@since      18/02/2020
@version    P12
/*/
//-------------------------------------------------------------------
Static Function ExcluiErro()

    Local oAuto := AutoTuco():New()
    Local nRecno
    Local aArq
    Local x

    nRecno := oAuto:PegaRecno(Alltrim(LOG->LOG_ERRO))

    If ExistDir("\tuco_erros\"+cValToChar(nRecno)+"\")
        aArq := Directory("/tuco_erros/"+cValToChar(nRecno)+"/*.*")
        For x := 1 To Len(aArq)
            If FErase("\tuco_erros\"+cValToChar(nRecno)+"\"+aArq[x][1]) != 0
                MsgStop(STR0052 + Str(FError()), STR0002) //"N�o foi poss�vel excluir o arquivo "
            Endif
        Next x
        If !DirRemove("\tuco_erros\"+cValToChar(nRecno)+"\")
            MsgStop(STR0115 + Str(FError()), STR0002) //"N�o foi poss�vel remover o diret�rio de armazenamento deste erro"
        Endif
    Endif

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} ValidDate
Valida se h� erros muito antigos na base

@type       Method

@author     Jo�o Pedro
@since      21/02/2020
@version    P12
/*/
//-------------------------------------------------------------------
Method ValidDate() Class Tuco

    Local nTime := 2
    Local nDiferenca
    Local nDetlzd := 0

    DbSelectArea("LOG")
    LOG->(DbGoTop())

    Do While LOG->(!Eof())

        nDiferenca := DateDiffYear(LOG->LOG_DT_FT, dDataBase)

        If nDiferenca > nTime
            nDetlzd++
        Endif
        LOG->(DbSkip())

    End do

    If nDetlzd > 0
        EECVIEW("H� " + cValToChar(nDetlzd) + " " + STR0120, STR0002) //"Erros com data de fontes antigos"
    Endif

Return Self