#Include 'Protheus.ch'
#Include 'Totvs.ch'
#Include 'Tuco.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} U_AutoDue
Analisa arquivos de retorno da DU-E

@type       User Function

@author     Jo�o Pedro
@since      16/01/2018
@version    P12
/*/
//-------------------------------------------------------------------
User Function AutoDue()

    Local oDue := DUE():New() //Instancia objeto de an�lise da DU-E
    oDue:paramDue()           //Chama tela de sele��o de arquivos da DU-E

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} DUE
Classe principal da an�lise de logs da DU-E

@type       Class

@author     Jo�o Pedro
@since      16/01/2018
@version    P12
/*/
//-------------------------------------------------------------------
Class DUE From Tuco

    Data aRet           as Array    //Necess�rio para n�o gerar error.log
    Data aErros         as Array    //Guarda erros que deram na transmiss�o
    Data cMsgSugestao   as String   //Sugest�o de solu��o para os erros
    /* Variaveis utilizadas para n�o mostrar o mesmo erro 2x em tela */
    Data lXSD           as Boolean 
    Data lCertificado   as Boolean
    Data lDeclarante    as Boolean
    Data lRetifica      as Boolean
    Data lDueGrande     as Boolean

    Method New() Constructor
    Method paramDue()               //Abre tela de sele��o de arquivos
    Method ReadArq()                //L� o arquivo de retorno da DU-E
    Method VerDados()               //Valida os dados de java e .jar da DU-E
    Method VerErro(cErro)           //Valida os erros da DU-E

End Class

//-------------------------------------------------------------------
/*/{Protheus.doc} New
Construtor para instanciar variaveis

@type       Method

@author     Jo�o Pedro
@since      16/01/2018
@version    P12
/*/
//-------------------------------------------------------------------
Method New() Class DUE

    ::aRet          := {}
    ::aErros        := {}
    ::cMsgSugestao  := ""
    ::lXSD          := .F.
    ::lCertificado  := .F.
    ::lDeclarante   := .F.
    ::lRetifica     := .F.
    ::lDueGrande    := .F.

Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} paramDue
Abre tela de sele��o de arquivos

@type       Method

@author     Jo�o Pedro
@since      16/01/2018
@version    P12

@return     Self
/*/
//-------------------------------------------------------------------
Method paramDue() Class DUE

    Local oDlg
    Local cDirArq       := Space(100)
    Local bDir          := {|| cDirArq := cGetFile("*.txt|*.log","Arquivos",1,"C:\",.F.,)}          //Tela de sele��o de arquivos
    Local bAction       := {|| oDlg:End() }                                                         //Bloco a ser executado quando clicado no bot�o Ok
    Local nLin := 25                                                                                //Linha inicial da janela de sele��o
    Local nCol := 12                                                                                //Coluna inicial da janela de sele��o
    Local lCancel       := .F.                                                                      //Define se salvou sem informar diret�rio
    Local bCancel       := {|| lCancel := .T., oDlg:End()}                                          //Bloco a ser executado quando clicado no bot�o Cancel

    DEFINE MSDIALOG oDlg TITLE "Escolha o arquivo" FROM 320, 400 TO 570, 720 OF oMainWnd PIXEL

        oPanel:= TPanel():New(0, 0, "", oDlg,, .F., .F.,,, 90, 165)
        oPanel:Align:= CONTROL_ALIGN_ALLCLIENT

        @ nLin, nCol Say "Arquivo ?" Size 160, 08 PIXEL OF oPanel
        nLin += 10
        @ nLin, nCol MsGet cDirArq Size 120, 08 PIXEL OF oPanel
        @ nLin, nCol+120 BUTTON "..." ACTION Eval(bDir) Size 10, 10 PIXEL OF oPanel

    ACTIVATE MSDIALOG oDlg ON INIT EnchoiceBar(oDlg, bAction, bCancel) CENTERED

    If !Empty(cDirArq)
        aAdd(::aRet, "\" + StrTokArr(cDirArq, "\")[Len(StrTokArr(cDirArq, "\"))])
        cFile := ::aRet[1]
        If ".log" $ cFile .OR. ".txt" $ cFile
            Processa({||::ReadArq(cDirArq)},STR0006,STR0007,.F.)                                    //Fun��o que ir� ler linha por linha do arquivo de retorno
        Else
            MsgStop(STR0008,STR0002)                                                                //Voc� n�o quer que eu leia arquivos com esta extens�o n� ?
            Return .F.
        Endif
    Elseif !lCancel
        MsgStop(STR0077, STR0002)                                                                   //Voc� n�o informa nenhum arquivo e quer que eu processe o que ?
    Endif

Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} ReadArq
L� arquivo de retorno da DU-E

@type       Method

@author     Jo�o Pedro
@since      16/01/2018
@version    P12

@param      cFile {String}, Nome do arquivo a ser analisado

@return     Self
/*/
//-------------------------------------------------------------------
Method ReadArq(cFile) Class DUE

    Local oFile                                                                                     //Objeto que conter� o FWFileReader
    Local cLinha                                                                                    //Guarda a linha atual do arquivo

    ProcRegua(1000)

    oFile := FWFileReader():New(cFile)                                                              //Instancia a class de leitura do arquivo
    If oFile:Open()                                                                                 //Abre o arquivo para leitura
        Do While oFile:hasLine()
            IncProc()
            cLinha := oFile:GetLine()                                                               //L� a linha atual do arquivo
            If "[Vers�o:" $ cLinha .AND. "suite" $ cLinha                                           //L� linha do .jar
                AADD(::aErros, cLinha)
            Endif
            If "[ERROR]" $ cLinha .AND. !"TrSwCrawler" $ cLinha .OR. "<error>" $ cLinha             //L� erros que deram no retorno
                AADD(::aErros, cLinha)
            Endif
            If "Java Runtime" $ cLinha                                                              //L� vers�o do Java
                AADD(::aErros, cLinha)
            Endif
            If "URL INFORMADA" $ cLinha                                                             //L� base para qual est� transmitindo
                AADD(::aErros, cLinha)
            Endif
        Enddo
        Processa({||::VerDados()},STR0006,STR0014)                                                  //Verifica os dados
        oFile:Close()                                                                               //Fecha o arquivo que est� lendo
    Endif

Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} VerDados
Valida vers�o .jar e java

@type       Method

@author     Jo�o Pedro
@since      16/01/2018
@version    P12

@return     Self
/*/
//-------------------------------------------------------------------
Method VerDados() Class DUE

    Local x                                                                                         //Variavel Local do For
    Local aVersao   := {}                                                                           //Guarda a vers�o do arquivo .jar
    Local aJava     := {}                                                                           //Guarda a vers�o do arquivo java
    Local cJava     := ""
    Local cVersao   := ""
    Local cUrl      := ""
    Local cMsg      := ""
    Local cMsgErro  := STR0054 + CRLF                                                               //***********Mensagens de erro***********
    Local cMsgJava  := ""

    For x := 1 To Len(::aErros)
        If "Java Runtime" $ ::aErros[x] .AND. Empty(cMsgJava)                                       
            aJava := Strtokarr(::aErros[x],"(")                                                     //Separa os dados do java
            cJava := Alltrim(Right(aJava[1],14))                                                    //Guarda vers�o do Java
            If "1.8.0_231" $ cJava                                                                  //Valida com a ultima vers�o do Java
                cMsgJava += "Java" + "  --->  " + "Encontra-se atualizado" + CRLF + CRLF
            Else
                cMsgJava += "Java" + "  --->  " + "Arquivo desatualizado" + CRLF + CRLF
            Endif
        Endif
        If "[Vers�o" $ ::aErros[x]
            aVersao := Strtokarr2( ::aErros[x], "suite")                                            //Separa os dados do .jar
            cVersao := Alltrim(StrTran(Strtokarr2(aVersao[1],"Vers�o:")[2],"("," "))                //Guarda vers�o do .jar
            If "3.1.0" $ cVersao                                                                    //Valida com a ultima vers�o do .jar
                cMsg += "Integrador .JAR" + "  --->  " + "Encontra-se atualizado" + CRLF + CRLF
            Else
                cMsg += "Integrador .JAR" + "  --->  " + "Arquivo desatualizado" + CRLF + CRLF
            Endif
        Endif
        If "URL INFORMADA" $ ::aErros[x]
            cUrl := Iif("val" $ Alltrim(StrTokArr2(::aErros[x],"URL INFORMADA:")[2]), cMsg += "Base DU-E  --->  TESTE" + CRLF + CRLF, cMsg += "Base DU-E  -->  PRODU��O" + CRLF + CRLF) //Verifica URL (Produ��o/Teste)
        Endif
        If "[ERROR]" $ ::aErros[x] .AND. !Alltrim(StrTokArr2(::aErros[x],"->")[2]) $ cMsgErro
            cMsgErro += CRLF + Alltrim(StrTokArr2(::aErros[x],"->")[2]) + CRLF                      //Separa os dados do erro
            ::VerErro(::aErros[x])                                                                  //M�todo que valida o erro
        Endif
    Next x

    cMsg += cMsgJava + cMsgErro                                                                     //Grava mensagens a serem apresentadas em tela

    If Empty(::cMsgSugestao)
        ::cMsgSugestao := STR0057
    Endif

    cMsg += CRLF + CRLF + STR0056 + CRLF + CRLF + ::cMsgSugestao                                    //Junta com as sugest�es de solu��o

    EECVIEW(cMsg,STR0055)                                                                           //Apresenta em tela

Return Self

//-------------------------------------------------------------------
/*/{Protheus.doc} VerDados
D� sugest�es para solu��o dos erros

@type       Method

@author     Jo�o Pedro
@since      16/01/2018
@version    P12

@param      cErro {String}, Erro a ser validado

@return     Self
/*/
//-------------------------------------------------------------------
Method VerErro(cErro) Class DUE

    Local aSug := {}                                        //Guarda sugest�o de solu��o
    Local cTag := ""

    If "definidas no XSD" $ cErro .AND. !::lXSD
        aSug := StrTokArr(cErro,"}")
        aSug := StrTokArr(aSug[1],"{")
        aSug := StrTokArr(aSug[2],":")
        cTag := aSug[Len(aSug)]
        ::cMsgSugestao += "O arquivo XML n�o est� completo ou a tag " + cTag + " est� incorreta." + CRLF + CRLF
        ::lXSD := .T.
    Endif

    If "Erro ao realizar autentica��o" $ cErro .AND. !::lCertificado
        ::cMsgSugestao += "N�o foi selecionado nenhum certificado ao transmitir a DU-E, ou o certificado escolhido � inv�lido" + CRLF + CRLF
        ::lCertificado := .T.
    Endif

    If "pessoa diferente do declarante" $ cErro .AND. !::lDeclarante
        ::cMsgSugestao += "Modificar o campo EEC_FORN para o de um fornecedor que tenha o mesmo CNPJ que a empresa que emitiu a nota pro Sefaz" + CRLF + CRLF
        ::lDeclarante := .T.
    Endif

    If "Falha ao processar a retifica��o" $ cErro .AND. !::lRetifica
        ::cMsgSugestao += "Informar o motivo da retifica��o da DU-E no sistema" + CRLF + CRLF
        ::lRetifica := .T.
    Endif

    If "vencido em" $ cErro
        ::cMsgSugestao += "O certificado " + Alltrim(StrTokArr2(Alltrim(StrTokArr2(cErro, "vencido")[1]), "certificado")) + " est� vencido, renove o mesmo e tente novamente" + CRLF + CRLF
    Endif

    If "maior do que o servidor deseja ou pode processar" $ cErro .AND. !::lDueGrande
        ::cMsgSugestao += "DU-E com quantidade de itens maior que o Siscomex aceita. Divida esta DU-E em 2 processos e tente novamente" + CRLF + CRLF
        ::lDueGrande := .T.
    Endif

Return Self